package Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.genieiot.marketing.AddRooms;
import com.genieiot.marketing.GetSwitchByRoomService;
import com.genieiot.marketing.LivingRoom;
import com.genieiot.marketing.SwitchSelectionActivity;
import com.genieiot.marketing.ChangeIPActivity;
import com.genieiot.marketing.HideList;
import com.genieiot.marketing.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Adapter.AdapterCustomRooms;
import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.HIDE;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.OPERATION;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_IMAGE_TYPE;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.ROOM_TYPE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static com.genieiot.marketing.MainActivity.isInternetAvailable;
import static com.genieiot.marketing.MainActivity.myToast;

/**
 * Created by Genie IoT on 9/13/2016.
 */
public class Rooms extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private View view;
    Context context;
    public static ArrayList<HashMap<String, String>> roomDatas;
    private ArrayList<String> categories;
    android.support.design.widget.FloatingActionButton mFabAdd;
    private EditText edtSwitchName;
    private String mSwitchName = "";
    private String mSwitchType = "";
    private DatabaseHandler db;
    private int count;
    private TextView txtCount;
    private SessionManager sessionManager;
    private String strWebserviceName = "";
    private ProgressDialog pDialog;
    private int methodType;
    private String roomId;
    RecyclerView lstRoom;
    AdapterCustomRooms adapter;
    private String messageType;
    LinearLayout linearLayout;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.rooms, container, false);
        db = new DatabaseHandler(getActivity());
        setHasOptionsMenu(true);
        roomDatas=new ArrayList<>();
        initWidgets();

        if (sessionManager.getUSERID().equals("")) {
            insertDefaultValuesInDatabase();
        }

        return view;
    }
    private void insertDefaultValuesInDatabase() {

        db = new DatabaseHandler(getActivity());

        if (db.getRoomTypeCount() < 1) {
            insertRoomType();
        }

        if (db.getSwitchTypeCount() < 1) {
            insertSwitchType();
        }

        if (db.getRoomCount() < 1) {
            insertRoom();
        }

        if (db.getSwitchCount() < 1) {
            insertSwitch();
        }
    }
    private void insertSwitch() {
        //this data is for hall
        HashMap<String, String> switcch = new HashMap<String, String>();
        for(int i=0;i<6;i++){
            switcch.put(SWITCH_NAME, "Bulb");
            switcch.put(ROOM_NAME, "Hall");
            switcch.put(SWITCH_TYPE_ID,(i+1)+"");
            switcch.put(SWITCH_STATUS, "0");
            switcch.put(ROOM_ID, "1");
            if(i==4 || i==5) {
                switcch.put(DIMMER_STATUS, "1");
            }else{
                switcch.put(DIMMER_STATUS, "0");
            }
            switcch.put(DIMMER_VALUE, "0");
            switcch.put(LOCK, "0");
            switcch.put(HIDE, "0");
            db.insertSwitch(switcch);
        }

        HashMap<String, String> switcch1 = new HashMap<String, String>();
        for(int i=0;i<6;i++){
            switcch1.put(SWITCH_NAME, "Bulb");
            switcch1.put(ROOM_NAME, "Kitchen");
            switcch1.put(SWITCH_TYPE_ID, (i+1)+"");
            switcch1.put(SWITCH_STATUS, "0");
            switcch1.put(ROOM_ID, "2");
            if(i==4 || i==5) {
                switcch1.put(DIMMER_STATUS, "1");
            }else{
                switcch1.put(DIMMER_STATUS, "0");
            }
            switcch1.put(DIMMER_VALUE, "0");
            switcch1.put(LOCK, "0");
            switcch1.put(HIDE, "0");
            db.insertSwitch(switcch1);
        }


        HashMap<String, String> switcch2 = new HashMap<String, String>();
        for(int i=0;i<6;i++){
            switcch2.put(SWITCH_NAME, "Bulb");
            switcch2.put(ROOM_NAME, "Bedroom");
            switcch2.put(SWITCH_TYPE_ID, (i+1)+"");
            switcch2.put(SWITCH_STATUS, "0");
            switcch2.put(ROOM_ID, "3");
            if(i==4 || i==5) {
                switcch2.put(DIMMER_STATUS, "1");
            }else{
                switcch2.put(DIMMER_STATUS, "0");
            }
            switcch2.put(DIMMER_VALUE, "0");
            switcch2.put(LOCK, "0");
            switcch2.put(HIDE, "0");
            db.insertSwitch(switcch2);
        }


    }
    private void insertRoom() {

        HashMap<String, String> room = new HashMap<String, String>();
        room.put(ROOM_NAME, "Hall");
        room.put(ROOM_TYPE_ID, "1");
        room.put(ROOM_ID,"1");
        db.insertRoom(room);

        HashMap<String, String> room1 = new HashMap<String, String>();
        room1.put(ROOM_NAME, "Kitchen");
        room1.put(ROOM_TYPE_ID, "2");
        room1.put(ROOM_ID,"2");
        db.insertRoom(room1);

        HashMap<String, String> room2 = new HashMap<String, String>();
        room2.put(ROOM_NAME, "Bedroom");
        room2.put(ROOM_TYPE_ID, "3");
        room2.put(ROOM_ID,"3");
        db.insertRoom(room2);
    }
    private void getRoomListServer() {

        if(URL_GENIE.equals(URL_GENIE_AWS)){
            messageType=INTERNET;
        }else{
            messageType=LOCAL_HUB;
        }

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("userId", sessionManager.getUSERID());
        jsonParams.put("messageFrom",messageType);
        strWebserviceName = "/room/getlistbyuser";
        methodType = Request.Method.POST;
        //callWebService(methodType, jsonParams, strWebserviceName);

        if(messageType.equals(INTERNET)){
         setRoomAdapter();
        }else {
            new AsyncRoomListTask().execute();
        }

    }
    class AsyncRoomListTask extends AsyncTask<Void,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants request=new Constants();
            String mResponse=null;
            try {
                JSONObject jMain=new JSONObject();
                jMain.put("userId",sessionManager.getUSERID());
                Log.d("Get room request ",jMain+"");
                mResponse=request.doPostRequest(URL_GENIE+"/room/getlistbyuser",jMain+"",sessionManager.getSecurityToken());
            }catch(Exception e){}
            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("Get room response : ",result+"");

            if(pDialog!=null && pDialog.isShowing()){
                pDialog.cancel();
            }

            if(result!=null){
                try {
                    JSONObject response=new JSONObject(result);

                    String status = response.getString("status");
                    String results = response.getString("result");
                    JSONArray resultArray = new JSONArray(results);

                    if (status.equals("SUCCESS")) {
                        createListForRoom(resultArray);
                        getActivity().startService(new Intent(getActivity(),GetSwitchByRoomService.class));
                        //setRoomAdapter();
                    }else{
                        Toast.makeText(context, ""+response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
            }

            setRoomAdapter();
        }
    }
    private void createListForRoom(JSONArray resultArray) {

        roomDatas.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            HashMap<String, String> roomType = new HashMap<String, String>();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(resultArray.get(i).toString());

                roomType.put(ROOM_ID, jsonObject.getString("id"));
                roomType.put(ROOM_NAME, jsonObject.getString("roomName"));
                roomType.put(ROOM_TYPE_ID, jsonObject.getString("roomType"));
                roomType.put(ROOM_IMAGE_TYPE, jsonObject.getString("roomImageId"));

                roomDatas.add(roomType);

                db.insertRoomNew(roomType);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }
    private void insertRoomType() {

        HashMap<String, String> roomtype = new HashMap<String, String>();

        roomtype.put("roomTypeName", "Hall");
        db.insertRoomType(roomtype);

        roomtype.put("roomTypeName", "Kitchen");
        db.insertRoomType(roomtype);


        roomtype.put("roomTypeName", "Bedroom");
        db.insertRoomType(roomtype);

        roomtype.put("roomTypeName", "Toliet");
        db.insertRoomType(roomtype);


    }
    private void insertSwitchType() {

        HashMap<String, String> type = new HashMap<String, String>();

        type.put("switchTypeName", "Lamp");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Fan");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Bulb");
        db.insertSwitchType(type);

        type.put("switchTypeName", "CFL Bulb");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Electric Stove");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Fridge");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Geyser");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Microwave");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Mosquito Machine");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Wall Socket");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Curtains");
        db.insertSwitchType(type);
        type.put("switchTypeName", "Iron machine");
        db.insertSwitchType(type);
        type.put("switchTypeName", "Washing Machine");
        db.insertSwitchType(type);
        type.put("switchTypeName", "Air Conditioner");
        db.insertSwitchType(type);

        type.put("switchTypeName", "Other");
        db.insertSwitchType(type);
    }
    public void onResume() {
        super.onResume();

//        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_up);
//        bottomUp.setDuration(500);
//        lstRoom.startAnimation(bottomUp);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message, new IntentFilter("NotificationSend"));

        if (sessionManager.getUSERID().equals("")) {
            getRoomListLocal();
            setRoomAdapter();

        } else {
            if (isInternetAvailable(getActivity())) {
                getRoomListServer();
            } else {
                setRoomAdapter();
                myToast(getActivity(), MESSAGE_INTERNET_CONNECTION);
            }
        }

    }
    private BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            setRoomAdapter();
        }
    };

    private void setRoomAdapter() {
        roomDatas= db.getRooms();
        adapter=new AdapterCustomRooms(getActivity(),roomDatas);
        lstRoom.setAdapter(adapter);
        lstRoom.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.notifyDataSetChanged();
    }
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.edit:
                if(sessionManager.getUserType().equals("2"))
                {
                    Toast.makeText(context, "Don't have authority to edit the room.",Toast.LENGTH_SHORT).show();
                }
                else {
                    addRoomDialog1("Edit", adapter.getPosition() + "");
                }
                break;

            case R.id.turnOffRoom:
                HashMap<String, String> map = (HashMap<String, String>) adapter.getItem(adapter.getPosition());

                ArrayList<HashMap<String, String>>  mapList=db.getSwitches(map.get(ROOM_ID));
                String mInstruction="";
                for(int i=0;i<mapList.size();i++)
                {
                    mInstruction += ":!200:!245:!" + mapList.get(i).get(SWITCH_TYPE_ID) + ":!" + "0" + ":!" + mapList.get(i).get(LOCK) + ":!168";
                }
                new LongOperation().execute(mInstruction);
                db.updateSwitchStatusByRoomId(mapList.get(0).get(ROOM_ID),"0");
                setRoomAdapter();
                break;

            case R.id.turnOnRoom:
                HashMap<String, String> map1 = (HashMap<String, String>) adapter.getItem(adapter.getPosition());
                ArrayList<HashMap<String, String>>  mapList1=db.getSwitches(map1.get(ROOM_ID));
                String mInstruction1="";
                Log.d("imageID", mapList1+"");
                for(int i=0;i<mapList1.size();i++)
                {
                    if(mapList1.get(i).get(DIMMER_STATUS).equals("1"))
                    {
                        mInstruction1+=":200:!245:!"+mapList1.get(i).get(SWITCH_TYPE_ID) + ":!" + mapList1.get(i).get(DIMMER_VALUE) + ":!" + mapList1.get(i).get(LOCK) + ":!168";
                    }
                    else {
                        mInstruction1 += ":!200:!245:!" + mapList1.get(i).get(SWITCH_TYPE_ID) + ":!" + "1" + ":!" + mapList1.get(i).get(LOCK) + ":!168";
                    }
                }
               new LongOperation().execute(mInstruction1);
                Log.d("Instruction",mInstruction1);
                db.updateSwitchStatusByRoomId(mapList1.get(0).get(ROOM_ID),"1");
                setRoomAdapter();
                break;


            case R.id.hide_room:
                alertHideSwitch();
                break;
            case R.id.hide:
                 createAlertPassdDialog();
                break;
            case R.id.menu_hide:
                 onClickOfHideMenu();
                break;
             default:
                return super.onContextItemSelected(item);
        }

        return true;
    }
    private class LongOperation extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                final String strConnection = params[0];
                final int roomId= Integer.parseInt(params[1]);
                setSwitchOn(strConnection);
                Log.d("Connection",strConnection);

            } catch (final Exception e) {
                e.printStackTrace();
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            setRoomAdapter();
        }


    }
    private void setSwitchOn(final String connection) throws Exception {

        Socket pingSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            pingSocket = new Socket(sessionManager.getRouterIP(),23);
            out = new PrintWriter(pingSocket.getOutputStream(), true);
        } catch (final IOException e) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Check hotspot", Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }
        out.println(connection);
        out.close();
        pingSocket.close();

    }


    private void alertTurnOffAllSwitch(final String mRoomId) {

        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_turnoff_all);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                //String roomid=mRoomId;
                new TurnOffAllSwitchAsyncTask().execute("0",mRoomId);
                dialog.dismiss();

            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void alertHideSwitch()    {

        if(sessionManager.getUserType().equals("2"))
        {
            Toast.makeText(context, "Don't have authority to hide the room.", Toast.LENGTH_SHORT).show();

        }
        else {
            final Dialog dialog = new Dialog(getActivity());
            // Include dialog.xml file
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_hide_switch);
            TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
            TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
            txtOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    onClickOfHideMenu();
                }
            });
            txtCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }
    private void onClickOfHideMenu() {
        DatabaseHandler db=new DatabaseHandler(getActivity());
        db.updateRoomHidestatus(roomDatas.get(adapter.getPosition()).get(ROOM_ID),"1");
        setRoomAdapter();
    }
    private void createAlertPassdDialog() {

        final Dialog dialog = new Dialog(getActivity());
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_ip_change);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        final EditText edtPassword = (EditText) dialog.findViewById(R.id.edtPassword);

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edtPass=edtPassword.getText().toString();
                if(edtPass.equals("")){
                    Toast.makeText(context, "Please enter some text.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(edtPass.equals(Constants.CHANGEIP_PASS)){
                    dialog.dismiss();
                    Intent intent=new Intent(getActivity(),ChangeIPActivity.class);
                    intent.putExtra(ROOM_ID,roomDatas.get(adapter.getPosition()).get(ROOM_ID));
                    startActivity(intent);
                }else{
                    Toast.makeText(context, "Please enter currect password.", Toast.LENGTH_SHORT).show();
                }


            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    private void getRoomListLocal() {

        try {
            roomDatas.clear();
            roomDatas = db.getRooms();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void initWidgets()  {

        context = getActivity();
        roomDatas = new ArrayList<HashMap<String, String>>();
        sessionManager = new SessionManager(getActivity());
        txtCount = (TextView) view.findViewById(R.id.txtCount);
        //lstRooms = (ListView) view.findViewById(R.id.listView);
        lstRoom= (RecyclerView) view.findViewById(R.id.lstRoom);
        linearLayout= (LinearLayout) view.findViewById(R.id.linearLayout);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);

    }

    public void onClick(View v) {

        switch (v.getId()) {
           case R.id.turnOffHome:
                  onIntentSelection("0");
               break;
            case R.id.turnOnHome:
                  onIntentSelection("1");
                break;
        }
    }

    private void onIntentSelection(String action){
        Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
        intentON.putExtra("ACTION",action);
        startActivity(intentON);
        //fab.close(true);
    }

    private void addRoomDialog1(String operation, String index) {

        Intent intent = new Intent(getActivity(), AddRooms.class);
        intent.putExtra(OPERATION, operation);

        if (operation.equals("Edit")) {
            intent.putExtra(ROOM_ID, roomDatas.get(Integer.parseInt(index)).get(ROOM_ID));

                intent.putExtra(ROOM_TYPE_ID, roomDatas.get(Integer.parseInt(index)).get(ROOM_TYPE_ID));
                intent.putExtra(ROOM_NAME, roomDatas.get(Integer.parseInt(index)).get(ROOM_NAME));
                intent.putExtra(ROOM_IMAGE_TYPE,roomDatas.get(Integer.parseInt(index)).get(ROOM_IMAGE_TYPE));

        }
        getActivity().startActivity(intent);

    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedText = parent.getSelectedItem().toString();

        if (position != 0) {
            edtSwitchName.setText(selectedText);
            return;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_hide:
                Intent intent=new Intent(getActivity(), HideList.class);
                intent.putExtra(ROOM_ID,"0");
                intent.putExtra(ROOM_NAME,"0");
                startActivity(intent);
                break;
            default:
                break;
        }
        return true;
    }

    private class TurnOffAllSwitchAsyncTask extends AsyncTask<String,Void,String> {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
          Constants req=new Constants();
            String response=null;
            try {
                //Parameter (Switch status,mRoomId)
                String mRequest = createJSONBody(params[0],params[1]);
                Log.d("Turn All Request ", mRequest);
               response=  req.doPostRequest(URL_GENIE + "/switch/changeStatusByRoom", mRequest,sessionManager.getSecurityToken());
            } catch (Exception e)
              {e.printStackTrace();}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(pDialog!=null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
            Log.d("Living Turn All Room ", result);
            if (result != null && !result.isEmpty()) {
                try {
                    JSONObject jResult = new JSONObject(result);
                    if (jResult.getString("status").equals("SUCCESS")) {
                        JSONArray jArrResult = new JSONArray(jResult.getString("result"));
                        for (int i = 0; i < jArrResult.length(); i++) {
                            JSONObject jSwitch = jArrResult.getJSONObject(i);
                            db.updateAllSwitchStatus(jSwitch.getString("siwtchid"),jSwitch.getString("status"));
                        }

                        Toast.makeText(context, "Action is done successfully.", Toast.LENGTH_SHORT).show();
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("NotificationSend"));
                    } else {
                        Toast.makeText(context, jResult.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                }  catch (Exception e) {
                }
                setRoomAdapter();
            } else {
                Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String createJSONBody(String switchStatus,String mRoomId) {
        JSONObject jMain = new JSONObject();
        try {
            jMain.put("roomId", mRoomId);
            jMain.put("onoffstatus", switchStatus);
            jMain.put("userId",sessionManager.getUSERID());
            jMain.put("messageFrom",messageType);
        } catch (Exception e) {e.printStackTrace();}
        return jMain + "";
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(message);
    }

}





