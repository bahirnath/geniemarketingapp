package Fragments;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.genieiot.marketing.AddNewTask;
import com.genieiot.marketing.AlarmReceiver;
import com.genieiot.marketing.R;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SCHEDULE_DATETIME;
import static Database.DatabaseHandler.SCHEDULE_ID;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.TIME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;


/**
 * Created by root on 10/12/16.
 */

public class Tab1 extends Fragment implements View.OnClickListener{

    private TimePicker mTimePicker;
    Button btn_scedule;
    final static int RQS_1 = 1;
    ImageView imgBack;
    Context context;
    View view;
    EditText edittxt_date,edittxt_time;
    Calendar myCalendar = Calendar.getInstance();
    Integer hours,minutes;
    private RadioGroup rgLock,rgSwitch;
    private RadioButton rbSwitchOn,rbSwitchOff,rbLockOn,rbLockOff;
    private ProgressDialog pDialog;
    private String mSwitchID="",mUserID="";
    private TextView txtText,txtTurnSwitch;
    private HashMap<String,String> mapList;
    private DatabaseHandler db;
    private String switchname;
    private String mScheduletime,mScheduledate,mScheduleTimeOnly,mRoomName;
    private String mSwitchStatus="";
    private SessionManager session;
    private HashMap<String, String> mapListEdit;
    private String mOpeartion;
    private String mScheduleId;
    private String messageType="";

    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.datetime, container, false);
        context=getActivity();
        btn_scedule = (Button) view.findViewById(R.id.btn_scedule);
        edittxt_date = (EditText) view.findViewById(R.id.edittxt_date);
        edittxt_time = (EditText) view.findViewById(R.id.edittxt_time);
        txtText= (TextView) view.findViewById(R.id.txtText);
        txtTurnSwitch= (TextView) view.findViewById(R.id.txtTurnSwitch);

        rgSwitch= (RadioGroup) view.findViewById(R.id.rgSwitch);
        rbSwitchOn= (RadioButton) view.findViewById(R.id.rbSwitchOn);
        rbSwitchOff= (RadioButton) view.findViewById(R.id.rbSwitchOff);
        mapList= (HashMap<String, String>) this.getArguments().getSerializable("SwitchInfo");
        mOpeartion=this.getArguments().getString("Operation");

        if(mOpeartion.equals("EDIT")){
            mapListEdit=(HashMap<String, String>) this.getArguments().getSerializable("EditInfo");
            edittxt_date.setText(mapListEdit.get(SCHEDULE_DATETIME));
            edittxt_time.setText(mapListEdit.get(TIME));
            mScheduleId=mapListEdit.get(SCHEDULE_ID);

            if(mapListEdit.get(SWITCH_STATUS).equals("0")){
                rbSwitchOff.setChecked(true);
            }else{
                rbSwitchOn.setChecked(true);
            }
        }

        session=new SessionManager(getActivity());
        db=new DatabaseHandler(getActivity());

        mSwitchID=mapList.get(SWITCH_ID);
        mUserID=session.getUSERID();

        //
        switchname=mapList.get("SwitchName");
        mRoomName=mapList.get("RoomName");
        txtText.setText("What do you want "+mapList.get("SwitchName")+" to do? ");
        txtTurnSwitch.setText("Turn"+ " "+switchname);

        btn_scedule.setOnClickListener(this);

        edittxt_date.setFocusable(false);
        edittxt_time.setFocusable(false);

        edittxt_date.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        edittxt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v != null) {
                    InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                TimePicker();

            }
        });


        return view;
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void TimePicker(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String mMin="";
                if(selectedMinute<10){
                    mMin="0"+selectedMinute;
                }else{
                    mMin=selectedMinute+"";
                }
                edittxt_time.setText( selectedHour + ":" + mMin);
                edittxt_time.setSelection(edittxt_time.getText().length());
                hours=selectedHour;
                minutes=selectedMinute;
            }
        }, hour, minute, true);//Yes 24 hour time

        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }
    private void updateLabel() {

        String myFormat ="yyyy-MM-dd";// "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

//        Calendar c = Calendar.getInstance();
//        Date strDate = sdf.parse(valid_until);
//        if (new Date().after(strDate)) {
//            catalog_outdated = 1;
//        }

        edittxt_date.setText(sdf.format(myCalendar.getTime()));
        edittxt_date.setSelection(edittxt_date.getText().length());

    }
    private void setAlarm(Calendar targetCal,String command,int alrmID){

        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra("Command", command);
        intent.putExtra("alarmId", alrmID);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alrmID, intent, 0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(), pendingIntent);

    }

    @Override
    public void onClick(View v) {

       // if(!edittxt_date.getText().toString().equals("") && !edittxt_time.getText().toString().equals("")) {
            switch (v.getId()) {
                case R.id.btn_scedule:

                    onClickOfSetTimer();

                    break;

            }
        }
//        else
//        {
//            Toast.makeText(context,"Please select Date and Time",Toast.LENGTH_LONG).show();
//        }


    private void onClickOfSetTimer() {

        String mLockStatus="";
        if(((RadioButton)rgSwitch.findViewById(R.id.rbSwitchOn)).isChecked()){
            mSwitchStatus="1";
        }
        if(((RadioButton)rgSwitch.findViewById(R.id.rbSwitchOff)).isChecked()){
            mSwitchStatus="0";
        }

        if(mSwitchStatus.equals("") && mLockStatus.equals("")){
            Toast.makeText(getActivity(), "Please select one of operation.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(edittxt_date.getText().toString().equals("")){
            Toast.makeText(context, "Please select date.", Toast.LENGTH_SHORT).show();
            return;

        }
        if(edittxt_time.getText().toString().equals("")){
            Toast.makeText(context, "Please select time.", Toast.LENGTH_SHORT).show();
            return;
        }


        mScheduledate=edittxt_date.getText().toString();
        mScheduleTimeOnly=edittxt_time.getText().toString();
        mScheduletime=edittxt_date.getText().toString()+" "+edittxt_time.getText().toString()+":00";

        try {
        SimpleDateFormat sdfCheck = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
         Date strDate = null;
            strDate = sdfCheck.parse(mScheduletime);
            if (new Date().after(strDate)) {
                Toast.makeText(context, "Please choose proper date and time.", Toast.LENGTH_SHORT).show();
            }
            else{
                if(session.getDemoUser().equals("DemoUser")){
                   setScheduler();
                }else {
                    if(mOpeartion.equals("EDIT")){
                        new AsyncEditScheduleTask().execute();
                    }else {
                        new AsyncScheduleTask().execute(mSwitchStatus, mLockStatus, mScheduletime);
                    }
                }
                //Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void setScheduler() {

        HashMap<String,String> mMap=new HashMap<>();
        mMap.put(SWITCH_ID,mSwitchID);
        mMap.put(SWITCH_NAME,switchname);
        mMap.put(SCHEDULE_DATETIME,mScheduledate);
        mMap.put(SWITCH_STATUS,mSwitchStatus);
        mMap.put(TIME,mScheduleTimeOnly);
        mMap.put(ROOM_NAME,mRoomName);
        mMap.put(IP,mapList.get(IP));
        DatabaseHandler db=new DatabaseHandler(context);

        db.insertSchedulerInfo(mMap);

        Intent alarmIntent = new Intent(getActivity(), AlarmReceiver.class);
        alarmIntent.putExtra(SWITCH_ID,mSwitchID);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, 0);

        AlarmManager manager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);

        manager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() +calulateTimeDiff(mScheduletime),pendingIntent);

        Intent intent=new Intent(getActivity(), AddNewTask.class);
        intent.putExtra("SwitchInfo",mapList);
        startActivity(intent);

    }
    private long calulateTimeDiff(String startDate) {
        String compareString="";
        Date d1 = null;
        Date d2 = null;
        long diff=0;
        Calendar calander = Calendar.getInstance();
        SimpleDateFormat formatS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//MM/dd/yyyy HH:mm:ss
        String stopDate= formatS.format(calander.getTime());
        try{

            d1 = formatS.parse(startDate);
            d2 = formatS.parse(stopDate);

            Log.d("Start Time",startDate);
            Log.d("Stop Time",stopDate);
            //in milliseconds
            diff = d1.getTime() - d2.getTime();
            Log.d("Diff...",diff+"");

        }catch(Exception e){}
        return diff;
    }

    private class AsyncScheduleTask extends AsyncTask<String,Void,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants res=new Constants();
            String response=null;

            if(URL_GENIE.equals(URL_GENIE_AWS)){
                messageType=INTERNET;
            }
            else{
                messageType=LOCAL_HUB;
            }

            try{
                JSONObject jReqBody=new JSONObject();
                jReqBody.put("switchId",mSwitchID);
                jReqBody.put("switchStatus",params[0]);
                jReqBody.put("lockStatus",params[1]);
                jReqBody.put("scheduleDateTime",params[2]);
                jReqBody.put("userId",mUserID);
                jReqBody.put("messageFrom",messageType);
                SessionManager session=new SessionManager(getActivity());
                response=res.doPostRequest(URL_GENIE+"/switch/scheduleswitch",""+jReqBody,session.getSecurityToken());

            }catch(Exception e){}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null){
                pDialog.cancel();
            }
            parseResult(result);
        }
    }

    private void parseResult(String result) {
        if(result!=null){
            try {
                JSONObject jObj = new JSONObject(result);
                if(jObj.getString("status").equals("SUCCESS")){

                    if(messageType.equals(INTERNET)){
                       navigateToScheduleList();
                    }


                    JSONObject jResult=new JSONObject(jObj.getString("result"));
                    DatabaseHandler db=new DatabaseHandler(context);
                    HashMap<String,String> mMap=new HashMap<>();
                    mMap.put(SWITCH_ID,mSwitchID);
                    mMap.put(SWITCH_NAME,switchname);
                    mMap.put(SCHEDULE_DATETIME,mScheduledate);
                    mMap.put(SWITCH_STATUS,mSwitchStatus);
                    mMap.put(TIME,mScheduleTimeOnly);
                    mMap.put(ROOM_NAME,mRoomName);
                    mMap.put(SCHEDULE_ID,jResult.getString("scheduleSwitchId"));

                    Toast.makeText(context, "Schedule is created successfully.", Toast.LENGTH_SHORT).show();
                    db.insertSchedulerInfo(mMap);

                    navigateToScheduleList();


                }else{
                    Toast.makeText(context, jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                }
            }catch(Exception e){}


        }else{
            Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    private void navigateToScheduleList() {
        Intent intent=new Intent(getActivity(), AddNewTask.class);
        intent.putExtra("SwitchInfo",mapList);
        intent.putExtra("Operation","ADD");
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    private class AsyncEditScheduleTask extends AsyncTask<String,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants req=new Constants();
            String response=null;

            if(URL_GENIE.equals(URL_GENIE_AWS)){
                messageType=INTERNET;
            }
            else{
                messageType=LOCAL_HUB;
            }

            try{
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("scheduleSwitchId",mScheduleId);
                jsonObject.put("switchId",mSwitchID);
                jsonObject.put("switchStatus",mSwitchStatus);
                jsonObject.put("scheduleDateTime",mScheduletime);
                jsonObject.put("userId",mUserID);
                jsonObject.put("messageFrom",messageType);
                response=req.doPostRequest(URL_GENIE+"/switch/editscheduleswitch",jsonObject+"",session.getSecurityToken());
            }catch(Exception e){}
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(pDialog!=null && pDialog.isShowing()){
               pDialog.cancel();
            }

            if(result!=null){
                try{
                    JSONObject jObj=new JSONObject(result);
                    if(jObj.getString("status").equals("SUCCESS")){
                        if(messageType.equals(INTERNET)){
                            navigateToScheduleList();
                        }

                        JSONObject jResult=new JSONObject(jObj.getString("result"));
                        db.updateSwitchSchedule(jResult.getString("scheduleSwitchId"),jResult.getString("scheduleDateTime"),jResult.getString("switchStatus"),mScheduleTimeOnly);
                        navigateToScheduleList();
                        Toast.makeText(context, "Schedule edit successfully.", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context, jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                    }

                }catch(Exception e){e.printStackTrace();}
            }else{
                Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
            }


        }

    }


}