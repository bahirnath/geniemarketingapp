package Fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.genieiot.marketing.R;

/**
 * Created by root on 9/22/16.
 */

public class DFragment extends DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialogfragment, container,false);
        getDialog().setTitle("DialogFragment Tutorial");

        return rootView;
    }
}