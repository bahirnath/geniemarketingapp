package Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.genieiot.marketing.LivingRoom;
import com.genieiot.marketing.R;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import Adapter.AdapterCustomRecent;
import Database.DatabaseHandler;
import Session.IOnClickOfSwitchChange;
import Session.NetworkConnectionInfo;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;

//import static com.genieiot.geniesmarthome.R.id.lstRecent;

/**
 * Created by Genie IoT on 9/13/2016.
 */
public class Recent extends Fragment implements IOnClickOfSwitchChange {

    private View view;
    private RecyclerView lstSwitches;
    Context context;

    //private AdapterRecent adapterSwitch;
    private AdapterCustomRecent adapterSwitch;
    private DatabaseHandler db;
    private ArrayList<HashMap<String, String>> listSwitchType;
    private ArrayList<HashMap<String, String>> switchDatas;

    int[] mSwitchON = new int[]{R.drawable.on_light_bulb, R.drawable.on_chandelier,R.drawable.on_cooler,R.drawable.on_desk_lamp,
            R.drawable.on_desktop,R.drawable.on_dish,R.drawable.on_exost,R.drawable.on_fan,R.drawable.on_refrigerator,
            R.drawable.on_microwave,R.drawable.on_mixer,R.drawable.on_purifier,R.drawable.on_socket,R.drawable.on_sound,
            R.drawable.on_stove,R.drawable.on_table_fan,R.drawable.on_television,R.drawable.on_tube,R.drawable.on_washing_machine,
            R.drawable.on_water_heater};


    int[] mSwitchOFF = new int[]{R.drawable.off_bulb, R.drawable.off_chandelier,R.drawable.off_cooler,R.drawable.off_desk_lamp,
            R.drawable.off_desktop_computer, R.drawable.off_dish,R.drawable.off_exost,R.drawable.off_fan,R.drawable.off_fridge,
            R.drawable.off_microwave,R.drawable.off_mixer,R.drawable.off_purifier,R.drawable.off_socket,R.drawable.off_sound,
            R.drawable.off_stove,R.drawable.off_table_fan,R.drawable.off_television,R.drawable.off_tube,R.drawable.off_washing_machine,
            R.drawable.off_water_heater};

    ArrayList<HashMap<String,Integer>> mSwitchONOFF=new ArrayList<>();

    String broker = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    MqttClient mqqtClient = null;
    MemoryPersistence persistence;
    SessionManager sessionManager;
    String mNetworkInfo;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recent, container, false);
        db = new DatabaseHandler(getActivity());
        initWidgets();
        sessionManager=new SessionManager(getActivity());
        broker="tcp://"+sessionManager.getRouterIP()+":1883";

        for(int i=0;i<mSwitchOFF.length;i++){
            HashMap<String,Integer> mMap=new HashMap<>();
            mMap.put("ON",mSwitchON[i]);
            mMap.put("OFF",mSwitchOFF[i]);
            mSwitchONOFF.add(mMap);

        }



        return view;
    }

    private void initWidgets() {
        context = getActivity();
        lstSwitches= (RecyclerView) view.findViewById(R.id.lstSwitches);
       // lstRecent = (ListView) view.findViewById(lstRecent);
    }

//    protected MqttClient getMqttConnection() throws MqttException {
//        if (mqqtClient != null) {
//            System.out.println("reusing conntion...");
//            return mqqtClient;
//        } else {
//             new AsyncMqttClientTask().execute();
//            return mqqtClient;
//        }
//    }

    @Override
    public void onResume() {

        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(messageRecent, new IntentFilter("MqttCallBack"));
        getSwitchesList();
        setSwitchAdapter();

        if(sessionManager.getDemoUser().equals("DemoUser")) {
//            if (NetworkConnectionInfo.CheckWifiConnection(context)) {
//                mNetworkInfo = "WIFI";
//                broker = "tcp://" + sessionManager.getRouterIP() + ":1883";
//            } else if (NetworkConnectionInfo.isInternetAvailable(context)) {
//                mNetworkInfo = "INTERNET";
//                broker = "tcp://geniewish.genieiot.com:1883";
//            }
        }
       // setMqttClient();
    }



    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(messageRecent);
    }

    public void getSwitchesList() {

        try {
            switchDatas = new ArrayList<HashMap<String,String>>();
            switchDatas.clear();
            switchDatas = db.getRecentSwitches();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setSwitchAdapter()
    {
        switchDatas = db.getRecentSwitches();
        Collections.reverse(switchDatas);
        adapterSwitch= new AdapterCustomRecent(getActivity(),switchDatas,mSwitchONOFF,Recent.this);
        lstSwitches.setLayoutManager(new LinearLayoutManager(context));
        lstSwitches.setAdapter(adapterSwitch);
        adapterSwitch.notifyDataSetChanged();

    }

    private BroadcastReceiver messageRecent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            //setActivityCount();
            setSwitchAdapter();

        }
    };


    @Override
    public void OnClickOfSwitchChange(HashMap<String, String> map) {
        String mqttMessage=null;
//        try {
//            mqttMessage = "$[" + map.get(SWITCH_TYPE_ID) + 0 + map.get(SWITCH_STATUS)+"0]";
//            Log.d("Living ROom ",mqttMessage);
//
//            String topic="";
//
//            if(mNetworkInfo.equals("INTERNET")){
//              topic=sessionManager.getTopicName();
//                mqttMessage="wish,"+map.get(IP)+"/out,"+mqttMessage;
//            }else{
//                topic=map.get(IP)+"/out";
//               // String topicIn=map.get(IP)+"/in";
//            }
//
//            MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());
//            message2.setQos(2);
//            Log.d("Message ",mqttMessage);
//            Log.d("Topic -->",topic);
//            Log.d("Broker ",broker);
//            if(mqqtClient!=null) {
//                getMqttConnection().publish(topic, message2);
//            }else{
//                new AsyncMqttClientTask().execute();
//            }
//
//            //getMqttConnection().subscribe(topic);
//            System.out.println("Message published");
//            db.updateSwitchStatus(map.get(SWITCH_ID),map.get(SWITCH_STATUS),map.get(DIMMER_VALUE));
//        } catch (MqttException e) {
//            e.printStackTrace();
//        }

        String mInstruction=":!200:!245:!"+map.get(SWITCH_TYPE_ID)+":!"+map.get(SWITCH_STATUS)+":!"+map.get(LOCK)+":!168";
        Log.d("Recent Command-->",mInstruction);
        db.updateSwitchStatus(map.get(SWITCH_ID), map.get(SWITCH_STATUS),map.get(DIMMER_VALUE));

        new LongOperation().execute(mInstruction);


    }


    private class LongOperation extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                final String strConnection = params[0];
                setSwitchOn(strConnection);

            } catch (final Exception e) {
                e.printStackTrace();
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return null;
        }
    }
    private void setSwitchOn(final String connection) throws Exception {

        Socket pingSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            pingSocket = new Socket("192.168.43.229",23);
            out = new PrintWriter(pingSocket.getOutputStream(), true);
        } catch (final IOException e) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Check hotspot", Toast.LENGTH_SHORT).show();
                }
            });
            return;

        }

        out.println(connection);
        out.close();
        pingSocket.close();

        adapterSwitch.notifyDataSetChanged();

    }

    @Override
    public void OnProgressChangeListener(HashMap<String, String> map) {

    }
}