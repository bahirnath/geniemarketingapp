package Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genieiot.marketing.ActivityService;
import com.genieiot.marketing.R;
import com.genieiot.marketing.SwitchSelectionActivity;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.readystatesoftware.viewbadger.BadgeView;

import net.hockeyapp.android.UpdateManager;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import Database.DatabaseHandler;
import Session.SessionManager;


/**
 * Created by Genie IoT on 9/13/2016.
 */
public class Home extends Fragment implements View.OnClickListener{

    private View view;
    private ViewPager viewPager;
    private android.support.design.widget.TabLayout tabLayout;
    private TextView txtCount,tab_badge;
    int count1;
    BadgeView badge;
    View target;
    FloatingActionMenu fab;
    FloatingActionButton turnOffHome,turnOnHome;
    SessionManager session;
    ImageView shield;
    boolean flag=false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home, container, false);

        fab= (FloatingActionMenu) view.findViewById(R.id.fabadd);
        //fab.setOnClickListener((View.OnClickListener) getActivity());
        fab.setClosedOnTouchOutside(true);

        turnOffHome= (FloatingActionButton) view.findViewById(R.id.turnOffHome);
        turnOnHome= (FloatingActionButton) view.findViewById(R.id.turnOnHome);

      //  shield= (ImageView) view.findViewById(R.id.shield);

        turnOffHome.setOnClickListener( this);
        turnOnHome.setOnClickListener(this);

      //  shield.setOnClickListener(this);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        txtCount= (TextView) view.findViewById(R.id.txtCount);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        session=new SessionManager(getActivity());

        createTabIcons();

        String strIP=getIP();
//        Log.d("IP of network..",strIP);

        //Constants.URL_GENIE = "http://"+session.getAPPURL()+":8080/GSmart_final_9jan/";



        DatabaseHandler db=new DatabaseHandler(getActivity());
        count1=db.getNotificationCount();

        return view;
    }

    private String getIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;

    }
    private void createTabIcons() {

        LinearLayout tabView=(LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
        TextView tv= (TextView) tabView.findViewById(R.id.tab);
        tv.setText("ROOMS");
        tabLayout.getTabAt(0).setCustomView(tabView);

        LinearLayout tabView1=(LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
        TextView tv1= (TextView) tabView1.findViewById(R.id.tab);
        tv1.setText("RECENTS");
        tabLayout.getTabAt(1).setCustomView(tabView1);

        LinearLayout tabView2=(LinearLayout)LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab,null);
        TextView tv2= (TextView) tabView2.findViewById(R.id.tab);
        tab_badge=(TextView)tabView2.findViewById(R.id.tab_badge);
        tv2.setText("ACTIVITY");
        tabLayout.getTabAt(2).setCustomView(tabView2);
        setActivityCount();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new Rooms(), "ROOMS");
        adapter.addFragment(new Recent(), "RECENTS");
        adapter.addFragment(new Activities(), "ACTIVITY");
      //  adapter.addFragment(new Count(),"count");
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if(position==2){
                   // DatabaseHandler database=new DatabaseHandler(getActivity());
                   // database.updateNotificationFlag();
                   // setActivityCount();
                    //getActivity().startService(new Intent(getActivity(),ActivityService.class));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.turnOffHome:
                onIntentSelection("0");
                break;
            case R.id.turnOnHome:
                onIntentSelection("1");
                break;
//            case R.id.shield:
//
//                if(flag)
//                {
//                    Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_up);
//                    bottomUp.setDuration(1000);
//                    shield.setImageResource(R.drawable.shielddeactive);
//                    shield.startAnimation(bottomUp);
//                    flag=false;
//                }
//                else
//                {
//                    Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_up);
//                    bottomUp.setDuration(1000);
//                    shield.setImageResource(R.drawable.shieldactivate);
//                    shield.startAnimation(bottomUp);
//                    flag=true;
//                }
//
//
//                break;
        }

    }
    private void onIntentSelection(String action){
        Intent intentON = new Intent(getActivity(), SwitchSelectionActivity.class);
        intentON.putExtra("ACTION",action);
        startActivity(intentON);
        fab.close(true);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public View addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position)
            {
                case 0:return mFragmentTitleList.get(position);
                case 1:return mFragmentTitleList.get(position);
                case 2:return mFragmentTitleList.get(position)+"("+count1+")";
            }

            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d("Home Fragment-->","onResume()");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(message, new IntentFilter("NotificationSend"));
        setTotalCount();
        UpdateManager.register(getActivity());

    }

    private void setTotalCount() {
        int count=0;
        DatabaseHandler db1 = new DatabaseHandler(getActivity());
        count = db1.getSwitchStatusCount();
        txtCount.setText(String.valueOf(count));
        viewPager.setCurrentItem(0);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(message);
    }

    private BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            setActivityCount();
            setTotalCountNew();
        }
    };

    private void setTotalCountNew() {
        int count=0;
        DatabaseHandler db1 = new DatabaseHandler(getActivity());
        count = db1.getSwitchStatusCount();
        txtCount.setText(String.valueOf(count));
    }

    public void setActivityCount(){

        if(getActivity()!=null) {
            DatabaseHandler database = new DatabaseHandler(getActivity());
            int countmsg = database.getUnReadNotificationMessage();

            if (countmsg > 0) {
                tab_badge.setVisibility(View.VISIBLE);
                tab_badge.setText(countmsg + "");
            } else {
                tab_badge.setVisibility(View.GONE);
            }
        }
    }
}
