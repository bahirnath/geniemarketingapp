package Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.R;

import Database.DatabaseHandler;

/**
 * Created by root on 28/11/16.
 */
public class Count extends Fragment {
    private View view;
    TextView txtCount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.count, container, false);
        initialize();
        return view;
    }

    private void initialize()
    {

        txtCount= (TextView) view.findViewById(R.id.txtcount);
        DatabaseHandler db=new DatabaseHandler(getActivity());

        int count=db.getNotificationCount();
        Log.d("s", String.valueOf(count));
        if(count==0)
        {
            Toast.makeText(getActivity(),"Notification panel is empty",Toast.LENGTH_LONG).show();
        }
        txtCount.setText(String.valueOf(count));
    }
}
