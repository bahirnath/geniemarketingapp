package Session;

import android.content.Context;
import android.net.ConnectivityManager;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by root on 9/21/16.
 */

public class Constants {

    // Shared pref mode
    public static int PRIVATE_MODE = 0;

    public static final String LOCAL_HUB="Localhub";
    public static final String INTERNET="Internet";

    // Sharedpref file name
    public static final String PREF_NAME = "SmartHome";

    // All Shared Preferences Keys
    public static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    public static final String CHANGEIP_PASS="g1234";

    public static final String MESSAGE_INTERNET_CONNECTION = "Check internet connection!!!";
    public static final String MESSAGE_TRY_AGAIN = "try again!!!";
   //public static final String URL_GENIE = "http://192.168.0.124:9095/"; //local url

    public static final int PORT = 8080;

//    public static final String URL_GENIE = "http://192.168.0.150:8080/gs-accessing-data-jpa-0.1.0/"; //local url

   //public static final String URL_GENIE = "http://Default-Environment.dbrqfrnqhm.us-west-2.elasticbeanstalk.com/"; //Amazon url
   // public static final String URL_GENIE = "http://192.168.0.119:8080/Final_GSmart1/"; //Raspberry

    public static String URL_GENIE = ""; //Raspberry
    public static String URL_GENIE_AWS = "http://backend.genieiot.com/"; //Raspberry


    public static boolean isInternetAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=UTF8");
    OkHttpClient client = new OkHttpClient();


    public String doPostRequest(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        client.setConnectTimeout(14, TimeUnit.SECONDS);  //Connect timeout
        client.setReadTimeout(14, TimeUnit.SECONDS);    //Socket timeout
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doGetRequest(String url,String auth_token) throws IOException {
        Request request = new Request.Builder()
                .header("X-AUTH-TOKEN",auth_token)
                .url(url)
                .build();
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doPostRequest(String url, String json,String auth_token) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .header("X-AUTH-TOKEN",auth_token)
                .url(url)
                .post(body)
                .build();
        client.setConnectTimeout(14, TimeUnit.SECONDS);  //Connect timeout
        client.setReadTimeout(14, TimeUnit.SECONDS);    //Socket timeout
        com.squareup.okhttp.Response response = client.newCall(request).execute();
        return response.body().string();
    }

}

