package DataModel;

/**
 * Created by root on 9/21/16.
 */

public class RecentData {

    String recentName;
    String switchStatus;
    String isDimmerAvailable;
    String dimmerValue;


    public String getRecentName() {
        return recentName;
    }

    public void setRecentName(String recentName) {
        this.recentName = recentName;
    }

    public String getSwitchStatus() {
        return switchStatus;
    }

    public void setSwitchStatus(String switchStatus) {
        this.switchStatus = switchStatus;
    }

    public String getIsDimmerAvailable() {
        return isDimmerAvailable;
    }

    public void setIsDimmerAvailable(String isDimmerAvailable) {
        this.isDimmerAvailable = isDimmerAvailable;
    }

    public String getDimmerValue() {
        return dimmerValue;
    }

    public void setDimmerValue(String dimmerValue) {
        this.dimmerValue = dimmerValue;
    }
}
