package DataModel;

/**
 * Created by root on 9/21/16.
 */

public class SwitchData {


    String switchId;
    String switchName;
    String switchType;
    String swichStatus;


    public String getSwitchId() {
        return switchId;
    }

    public void setSwitchId(String switchId) {
        this.switchId = switchId;
    }

    public String getSwichStatus() {
        return swichStatus;
    }

    public void setSwichStatus(String swichStatus) {
        this.swichStatus = swichStatus;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }

    public String getSwitchType() {
        return switchType;
    }

    public void setSwitchType(String switchType) {
        this.switchType = switchType;
    }
}
