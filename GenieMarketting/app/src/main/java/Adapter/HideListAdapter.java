package Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.genieiot.marketing.HideList;
import com.genieiot.marketing.R;

import java.util.ArrayList;
import java.util.HashMap;

import static Database.DatabaseHandler.SWITCH_TYPE_ID;

/**
 * Created by root on 24/11/16.
 */

public class HideListAdapter extends RecyclerView.Adapter<HideListAdapter.MyViewHolder> {


    private HideList context;
    private ArrayList<HashMap<String, String>> arrayList;

    int[] mSwitchON = new int[]{R.drawable.on_light_bulb, R.drawable.on_chandelier,R.drawable.on_cooler,R.drawable.on_desk_lamp,
            R.drawable.on_desktop,R.drawable.on_dish,R.drawable.on_exost,R.drawable.on_fan,R.drawable.on_refrigerator,
            R.drawable.on_microwave,R.drawable.on_mixer,R.drawable.on_purifier,R.drawable.on_socket,R.drawable.on_sound,
            R.drawable.on_stove,R.drawable.on_table_fan,R.drawable.on_television,R.drawable.on_tube,R.drawable.on_washing_machine,
            R.drawable.on_water_heater};

    public HideListAdapter(HideList context, ArrayList<HashMap<String, String>> arrayList) {
         this.context=context;
         this.arrayList=arrayList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hide, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        holder.mTxtItemSwitch.setText(arrayList.get(position).get("SwitchName"));
        holder.txtItemRoom.setText(arrayList.get(position).get("RoomName"));

        String mImage=arrayList.get(position).get(SWITCH_TYPE_ID);

        holder.mImgItemSwitch.setImageResource(mSwitchON[Integer.parseInt(mImage)]);

       // holder.mImgItemSwitch.setImageResource(Integer.parseInt(arrayList.get(position).get("RoomImageType")));

        holder.mTxtUnhide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.onClickListner(arrayList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mImgItemSwitch;
        TextView mTxtItemSwitch,mTxtUnhide,txtItemRoom;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            mImgItemSwitch= (ImageView) itemView.findViewById(R.id.imgItemSwitch);
            mTxtItemSwitch= (TextView) itemView.findViewById(R.id.txtItemSwitch);
            mTxtUnhide= (TextView) itemView.findViewById(R.id.mTxtUnhide);
            txtItemRoom= (TextView) itemView.findViewById(R.id.txtItemRoom);

        }
    }



}
