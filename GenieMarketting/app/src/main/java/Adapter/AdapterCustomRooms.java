package Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.LivingRoom;
import com.genieiot.marketing.R;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import Database.DatabaseHandler;
import Session.Constants;
import Session.NetworkConnectionInfo;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.HIDE;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_IMAGE_TYPE;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;

/**
 * Created by root on 26/12/16.
 */

public class AdapterCustomRooms extends RecyclerView.Adapter<AdapterCustomRooms.MyViewHolder> {
    private final ArrayList<HashMap<String, String>> roomDatas;
    Context context;
    int[] myImageList = new int[]{R.drawable.bedroom, R.drawable.livingroom,R.drawable.kitchen,R.drawable.bathroom,
            R.drawable.balcony,R.drawable.living_room ,R.drawable.terrace,R.drawable.stdyroom,R.drawable.kidsbedroom,R.drawable.parking};
    DatabaseHandler db;
    private int mPosition;
    SessionManager session;

    public AdapterCustomRooms(Context context,ArrayList<HashMap<String, String>> roomDatas){
        this.roomDatas = roomDatas;
        this.context=context;
        db=new DatabaseHandler(context);
        session=new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_board, parent, false);
        return new AdapterCustomRooms.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.tv.setText(roomDatas.get(position).get(ROOM_NAME));

        String mImage=roomDatas.get(position).get(ROOM_IMAGE_TYPE);

        holder.img.setImageResource(myImageList[Integer.parseInt(mImage)]);

        int mRoomOnCount=db.getSwitchRoomStatusCount(roomDatas.get(position).get(ROOM_ID));

        if(mRoomOnCount>0){
            holder.tvRoomCount.setText(mRoomOnCount+"");
            holder.tvRoomCount.setVisibility(View.VISIBLE);
        }else{
            holder.tvRoomCount.setText(mRoomOnCount+"");
            holder.tvRoomCount.setVisibility(View.INVISIBLE);
        }


        final MyViewHolder mholder=holder;
        holder.itemboard.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(mholder.getAdapterPosition());
                return false;
            }
        });

        holder.itemboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(session.getUSERID().equals("")){
                    ArrayList<HashMap<String,String>> switchMapList=db.getSwitches(roomDatas.get(position).get(ROOM_ID));
                    if(switchMapList.size()>0){
                             Activity activity = (Activity) context;
                            Intent intent = new Intent(context, LivingRoom.class);
                            intent.putExtra(ROOM_ID, roomDatas.get(position).get(ROOM_ID));
                            intent.putExtra(ROOM_NAME, roomDatas.get(position).get(ROOM_NAME));
                            intent.putExtra("NetWorkInfo","WIFI");
                            activity.startActivity(intent);
                            activity.overridePendingTransition(R.anim.enter, R.anim.exit);

                    }else{
                        if(roomDatas.get(position).get(ROOM_ID).equals("1")){
                            new AsyncSwitchDataTask().execute(session.getFirstString(),position+"");
                        }else if(roomDatas.get(position).get(ROOM_ID).equals("2")){
                            new AsyncSwitchDataTask().execute(session.getSecondString(),position+"");
                        }else if(roomDatas.get(position).get(ROOM_ID).equals("3")){
                            new AsyncSwitchDataTask().execute(session.getThirdString(),position+"");
                        }
                    }


                }else {
                    new AsyncTaskCheckHub().execute(position);
                }
            }
        });

    }

    private String getRoomIP(int pos) {
        String IP=null;
        if(roomDatas.get(pos).get(ROOM_ID).equals("1")){
            return session.getFirstString();
        }else if(roomDatas.get(pos).get(ROOM_ID).equals("2")){
            return session.getSecondString();
        }else if(roomDatas.get(pos).get(ROOM_ID).equals("3")){
            return session.getThirdString();
        }
        return IP;
    }

    @Override
    public int getItemCount() {
        return roomDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        TextView tv;
        TextView tvRoomCount;
        ImageView img;
        LinearLayout itemboard;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.txtItemBoard);
            tvRoomCount = (TextView) itemView.findViewById(R.id.tvRoomCount);
            img = (ImageView) itemView.findViewById(R.id.imgItemBoard);
            itemboard = (LinearLayout) itemView.findViewById(R.id.itemboard);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0,R.id.edit, 0, "Edit");
            menu.add(0,R.id.hide_room,0,"Hide");
            menu.add(0,R.id.turnOffRoom,0,"Turn OFF All");
            menu.add(0,R.id.turnOnRoom,0,"Turn ON All");

           // menu.add(0,R.id.turnoffall,0,"Turn OFF Room");

            if(session.getUSERID().equals("")){
           //  menu.add(0,R.id.hide,0,"Change Panel");
          //   menu.add(0,R.id.menu_hide,0,"Hide");
            }

        }
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return roomDatas.get(position);
    }

    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        this.mPosition = position;
    }

    class AsyncSwitchDataTask extends AsyncTask<String,Void,String>{
        ProgressDialog pDialog;
        String mFinalResult;
        int curPosition;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Socket pingSocket = null;
            PrintWriter out = null;
//            BufferedReader inFromServer = null;
//
//            try {
                String mIP=params[0];
                curPosition=Integer.parseInt(params[1]);

            Socket socket = null;

            try {

                socket = new Socket(mIP,23);

                out = new PrintWriter(socket.getOutputStream(), true);
                out.println(":f");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                byte[] buffer = new byte[1024];

                int bytesRead;
                InputStream inputStream = socket.getInputStream();

         /*
          * notice: inputStream.read() will block if no data return
          */
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    byteArrayOutputStream.write(buffer, 0, bytesRead);
                    mFinalResult += byteArrayOutputStream.toString("UTF-8");
                    Log.d("Socket",mFinalResult);
                    break;
                }

            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                mFinalResult = "UnknownHostException: " + e.toString();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                mFinalResult = "IOException: " + e.toString();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                        out.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            return mFinalResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null && pDialog.isShowing()){
               pDialog.cancel();
            }
            result="123456";
            if(result!=null){
                try{
                   String[] roomArr=result.split("-");
                   if(roomArr.length>3){
                       String roomString=roomArr[2];
                       Log.d("Room Data String : ",""+roomString);
                       for(int i=0;i<6;i++){
                        if(Integer.parseInt(String.valueOf(roomString.charAt(i)))>0){
                            if(i==4 || i==5){
                                insertRoomInfo((i + 1), curPosition, "1");
                            }else {
                                insertRoomInfo((i + 1), curPosition, "0");
                            }
                        }

                       }
                   }

                    Activity activity = (Activity) context;
                    Intent intent = new Intent(context, LivingRoom.class);
                    intent.putExtra(ROOM_ID, roomDatas.get(curPosition).get(ROOM_ID));
                    intent.putExtra(ROOM_NAME, roomDatas.get(curPosition).get(ROOM_NAME));
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);

                }catch(Exception e){}



            }else{
                Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void insertRoomInfo(int mSwitchType,int position,String mDimmerStatus) {
        HashMap<String, String> switcch = new HashMap<String, String>();
        switcch.put(SWITCH_NAME, "Bulb");
        switcch.put(ROOM_NAME, roomDatas.get(position).get(ROOM_NAME));
        switcch.put(SWITCH_TYPE_ID,mSwitchType+"");
        switcch.put(SWITCH_STATUS, "0");
        switcch.put(ROOM_ID,roomDatas.get(position).get(ROOM_ID));
        switcch.put(DIMMER_STATUS,mDimmerStatus);
        switcch.put(DIMMER_VALUE, "0");
        switcch.put(LOCK, "0");
        switcch.put(HIDE, "0");

        Log.d("Switch Insert Data ",""+switcch);

        db.insertSwitch(switcch);
    }


    private class LongOperation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                Thread.sleep(150);
            }catch (Exception e){}
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("TryDemoSwitchAdapter ","onPostExecute()");
            notifyDataSetChanged();
            if (context instanceof LivingRoom)
            {
                ((LivingRoom) context).settextCount(0);
            }

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String strConnection = params[0];
                String ip=params[1];
                setSwitchOn(strConnection,ip);

            } catch (final Exception e) {

                e.printStackTrace();

                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
            return null;
        }
    }
    private void setSwitchOn(final String connection,String ip) throws Exception {

        Socket pingSocket=null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            pingSocket = new Socket(ip,23);
            out = new PrintWriter(pingSocket.getOutputStream(), true);

        } catch (final IOException e) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }

        out.println(connection);

        try{
            Thread.sleep(300);
        }catch(Exception e){e.printStackTrace();}

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
        byte[] buffer = new byte[1024];
        int bytesRead;
        InputStream inputStream = pingSocket.getInputStream();
        String mFinalResult="";
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, bytesRead);
            mFinalResult += byteArrayOutputStream.toString("UTF-8");
            Log.d("Room Response : ",mFinalResult);
            break;
        }

        Log.d("Success ",connection);
        out.close();

        pingSocket.close();

    }


    private class AsyncTaskCheckHub extends AsyncTask<Integer,Void,String> {
        int curPosition;
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Integer... params) {
            curPosition = params[0];
            String response = "";
            Constants req = new Constants();
            try {
                String appUrl="http://"+session.getRouterIP()+":8080/GSmart_final_9jan/home/verifygeniehub";
                Log.d("Room Adapter URL",appUrl);
                response = req.doGetRequest(appUrl);
            } catch (Exception e) {
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }
            if (result != null && !result.isEmpty()) {
                parseHubResultData(result, curPosition);
            } else {
                if (NetworkConnectionInfo.isInternetAvailable(context)) {
                    Activity activity = (Activity) context;
                    Intent intent = new Intent(context, LivingRoom.class);
                    intent.putExtra(ROOM_ID, roomDatas.get(curPosition).get(ROOM_ID));
                    intent.putExtra(ROOM_NAME, roomDatas.get(curPosition).get(ROOM_NAME));
                    intent.putExtra("NetWorkInfo", "INTERNET");
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        }

        private void parseHubResultData(String result, int position) {
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.getString("status").equals("SUCCESS")) {
                    Activity activity = (Activity) context;
                    Intent intent = new Intent(context, LivingRoom.class);
                    intent.putExtra(ROOM_ID, roomDatas.get(position).get(ROOM_ID));
                    intent.putExtra(ROOM_NAME, roomDatas.get(position).get(ROOM_NAME));
                    intent.putExtra("NetWorkInfo", "WIFI");
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            } catch (Exception e) {
            }
        }
    }
}
