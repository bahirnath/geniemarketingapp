package Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.LivingRoom;
import com.genieiot.marketing.R;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Database.DatabaseHandler;
import Session.SessionManager;

import static Database.DatabaseHandler.CREATED_BY;
import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Database.DatabaseHandler.TIME;

/**
 * Created by root on 30/12/16.
 */

public class TryDemoSwitchAdpater extends RecyclerView.Adapter<TryDemoSwitchAdpater.MyViewHolder> {

    private static final int PORT = 23;
    private String IP_ADDRESS = "";
    Context context;
    ArrayList<HashMap<String, String>> switchDatas;
    SessionManager session;
    private List<Boolean> mSbStates;
    int step = 1;
    int max = 100;
    int min = 0;
    DatabaseHandler db;
    LivingRoom thisLiving;
    private int mPosition;
    private ArrayList<HashMap<String,Integer>> switchONFF;

    public TryDemoSwitchAdpater(Context context, ArrayList<HashMap<String, String>> arrayList,ArrayList<HashMap<String,Integer>> map,String IP_ADDRESS) {

        this.context=context;
        this.switchDatas=arrayList;
        session=new SessionManager(context);
        db=new DatabaseHandler(context);
        thisLiving= (LivingRoom)context;
        switchONFF=map;
        this.IP_ADDRESS=IP_ADDRESS;
        mSbStates = new ArrayList<>(getItemCount());
        for (int i = 0; i < getItemCount(); i++) {
            mSbStates.add(false);
        }

    }

    public TryDemoSwitchAdpater.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.switch_item_row, parent, false);
        return new TryDemoSwitchAdpater.MyViewHolder(itemView);
    }

    public void onBindViewHolder(final TryDemoSwitchAdpater.MyViewHolder holder, final int position)
    {
        holder.btnSwitchButton.setOnCheckedChangeListener(null);
        //holder.btnSwitchButton.setCheckedImmediately(mSbStates.get(position));
        //set component value
        holder.tvSwitchName.setText(switchDatas.get(position).get(SWITCH_NAME));
        if (switchDatas.get(position).get(DIMMER_STATUS).equals("1")) {
            int iDimmer=Integer.parseInt(switchDatas.get(position).get(DIMMER_VALUE));
            holder.layoutDimmer.setVisibility(View.VISIBLE);
            int dimmervalueset=Integer.parseInt(((iDimmer*100)/75)+"");
            holder.seekDimmer.setProgress(dimmervalueset);
            holder.seekDimmer.setIndicatorPopupEnabled(true);
        } else {
            holder.seekDimmer.setIndicatorPopupEnabled(false);
            holder.layoutDimmer.setVisibility(View.GONE);
        }

        if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
            holder.btnSwitchButton.setChecked(false);
            holder.imgSwitchImage.setImageResource(switchONFF.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("OFF"));
        } else {
            holder.btnSwitchButton.setChecked(true);
            holder.imgSwitchImage.setImageResource(switchONFF.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("ON"));
        }

        if (switchDatas.get(position).get(LOCK).toString().equals("1"))
        {   holder.lock.setVisibility(View.VISIBLE);   }
        else
        {   holder.lock.setVisibility(View.INVISIBLE); }


        if (session.getUserType().equals("2"))
        { holder.lock.setEnabled(false); }
        else
        {
            holder.lock.setEnabled(true);
        }

        holder.btnSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mSbStates.set(position, isChecked);
                HashMap<String,String> mMap=switchDatas.get(position);
                if(isChecked){
                    mMap.put(SWITCH_STATUS,"1");
                }else{
                    mMap.put(SWITCH_STATUS,"0");
                }
                switchDatas.set(position,mMap);
                thisLiving.OnClickOfSwitchChange(mMap);
                insertRecent(position,mMap.get(SWITCH_STATUS),IP_ADDRESS);

            }
        });

        holder.seekDimmer.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                //holder.seekDimmer.setProgress(seekBar.getProgress());
                //holder.seekDimmer.setT
                HashMap<String, String> mMap = switchDatas.get(position);
                mMap.put(DIMMER_VALUE, ((seekBar.getProgress()*75)/100)+"");
                if(seekBar.getProgress()>0) {
                    mMap.put(SWITCH_STATUS, "1");
                }else{
                    mMap.put(SWITCH_STATUS, "0");
                }
                thisLiving.OnProgressChangeListener(mMap);
                insertRecent(position,mMap.get(SWITCH_STATUS),IP_ADDRESS);
                notifyItemChanged(position);

//                String switchIds = switchDatas.get(position).get(SWITCH_ID);
//                db.updateDimmerValue(seekBar.getProgress()+"",switchIds);
//                View view=holder.btnSwitchButton;
//
//                if(isInternetAvailable(context)){
//                    HashMap<String,String> mMap=switchDatas.get(position);
//                    if(seekBar.getProgress()>0) {
//                        holder.btnSwitchButton.setChecked(true);
//                        mMap.put(SWITCH_STATUS,"1");
//                        mMap.put(DIMMER_VALUE,seekBar.getProgress()+"");
//                        Log.d("Switch Info",""+mMap);
//                        switchDatas.set(position,mMap);
//                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"1",seekBar.getProgress()+"");
//                        insertRecent(position,"1",IP_ADDRESS);
//                        String strDimmer=":!200:!245:!"+switchDatas.get(position).get(SWITCH_TYPE_ID)+":!0:!"+switchDatas.get(position).get(LOCK)+":!168";
//                        String strConnection=strDimmer+":!200:!245:!"+switchDatas.get(position).get(SWITCH_TYPE_ID)+":!"+seekBar.getProgress()+":!"+switchDatas.get(position).get(LOCK)+":!168";
//                        Log.d("IP_ADDRESS",IP_ADDRESS);
//                        Log.d("Switch ON ",strConnection);
//                        new LongOperation().execute(strConnection);
//                    }else{
//                        mMap.put(SWITCH_STATUS,"0");
//                        Log.d("Switch Info",""+mMap);
//                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"0",seekBar.getProgress()+"");
//                        switchDatas.set(position,mMap);
//                        holder.btnSwitchButton.setChecked(false);
//                        insertRecent(position,"1",IP_ADDRESS);
//                        String strDimmer=":!200:!245:!"+switchDatas.get(position).get(SWITCH_TYPE_ID)+":!0:!"+switchDatas.get(position).get(LOCK)+":!168";
//                        String strConnection=strDimmer+":!200:!245:!"+switchDatas.get(position).get(SWITCH_TYPE_ID)+":!"+seekBar.getProgress()+":!"+switchDatas.get(position).get(LOCK)+":!168";
//                        Log.d("IP_ADDRESS",IP_ADDRESS);
//                        Log.d("Switch ON ",strConnection);
//                        new LongOperation().execute(strConnection);
//                    }
//
//                  //  db.updateSwitchStatus(mMap.get(SWITCH_ID),jData.getString("state"),jData.getString("dimmerValue"));
//                  //  insertRecent(position,jData.getString("state"));
//
//
//                }else{
//                    Toast.makeText(context,"Please check internet Connection.", Toast.LENGTH_SHORT).show();
//                }
            }
        });

        holder.llMainScreen.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return switchDatas.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return switchDatas.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        TextView tvSwitchName;
        ImageView imgSwitchImage, lock;
        SwitchCompat btnSwitchButton;
        LinearLayout layoutDimmer, llLinear;
        DiscreteSeekBar seekDimmer;
        TextView txtRangeValue;
        LinearLayout llMainScreen;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvSwitchName = (TextView) itemView.findViewById(R.id.tvSwitchName);
            llLinear= (LinearLayout) itemView.findViewById(R.id.llLinear);
            txtRangeValue = (TextView) itemView.findViewById(R.id.txtRange);
            imgSwitchImage = (ImageView) itemView.findViewById(R.id.imgSwitchImage);
            btnSwitchButton = (SwitchCompat) itemView.findViewById(R.id.btnSwitchButton);
            seekDimmer = (DiscreteSeekBar) itemView.findViewById(R.id.seekDimmer);
            lock = (ImageView) itemView.findViewById(R.id.imglock);
            layoutDimmer = (LinearLayout) itemView.findViewById(R.id.layoutDimmer);
            llLinear = (LinearLayout) itemView.findViewById(R.id.llLinear);
            llMainScreen = (LinearLayout) itemView.findViewById(R.id.llMainScreen);
            seekDimmer.setMax((max - min) / step);
            itemView.setOnCreateContextMenuListener(this);


        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0,R.id.edit, 0, "Edit");
            menu.add(0, R.id.hide, 0, "Hide");
            if(switchDatas.get(getAdapterPosition()).get(LOCK).equals("0")) {
                menu.add(0, R.id.lock, 0, "Lock");
            }else{
                menu.add(0,R.id.lock,0,"Unlock");
            }
            //menu.add(0,R.id.schedule, 0, "Schedule");
        }
    }


    public int getPosition() {
        return mPosition;
    }

    public void setPosition(int position) {
        this.mPosition = position;
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                Thread.sleep(150);
            }catch (Exception e){}

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("TryDemoSwitchAdapter ","onPostExecute()");
            notifyDataSetChanged();
            if (context instanceof LivingRoom)
            {
                ((LivingRoom) context).settextCount(0);
            }

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String strConnection = params[0];
                setSwitchOn(strConnection);

            } catch (final Exception e) {

                e.printStackTrace();

                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
            return null;
        }
    }
    private void setSwitchOn(final String connection) throws Exception {

        Socket pingSocket=null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            pingSocket = new Socket(IP_ADDRESS, PORT);
            out = new PrintWriter(pingSocket.getOutputStream(), true);

        } catch (final IOException e) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }

        out.println(connection);
        Log.d("Success ",connection);
        out.close();

        pingSocket.close();

    }

    private void insertRecent(int position, String mStatus,String IPAddress) {

        Calendar calander;
        String time;
        calander = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        time= simpleDateFormat.format(calander.getTime());

        HashMap<String, String> switcch = new HashMap<String, String>();
        switcch.put(SWITCH_ID, switchDatas.get(position).get(SWITCH_ID));
        switcch.put(SWITCH_NAME, switchDatas.get(position).get(SWITCH_NAME));
        switcch.put(SWITCH_TYPE_ID, switchDatas.get(position).get(SWITCH_TYPE_ID));
        switcch.put(SWITCH_STATUS,mStatus);
        switcch.put(ROOM_ID, switchDatas.get(position).get(ROOM_ID));
        switcch.put(DIMMER_STATUS, switchDatas.get(position).get(DIMMER_STATUS));
        switcch.put(DIMMER_VALUE, switchDatas.get(position).get(DIMMER_VALUE));
        switcch.put(ROOM_NAME,switchDatas.get(position).get(ROOM_NAME));
        switcch.put(SWITCH_IMAGE_ID,switchDatas.get(position).get(SWITCH_IMAGE_ID));
        switcch.put(TIME,time);
        switcch.put(IP,IPAddress);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        String formattedDate=dateFormat.format(date);
        switcch.put(CREATED_BY,formattedDate);

        db.insertRecentSwitch((switcch));
    }
}

