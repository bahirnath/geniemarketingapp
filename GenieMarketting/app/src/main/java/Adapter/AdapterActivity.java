package Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.genieiot.marketing.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import Session.SessionManager;

import static Database.DatabaseHandler.ACTIVITY_TIME;
import static Database.DatabaseHandler.NOTIFICATION_MSG;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.USER_IMAGE;

/**
 * Created by root on 9/20/16.
 */

public class AdapterActivity extends BaseAdapter {

    Context context;
    int[] imageId;
    private static LayoutInflater inflater = null;
    ArrayList<LinkedHashMap<String,String>> msgList;
    SessionManager sessionManager;

    public AdapterActivity(Context context, int[] prgmImages, ArrayList<LinkedHashMap<String,String>> msgList) {
        // TODO Auto-generated constructor stub
        this.context = context;
        imageId = prgmImages;
        this.msgList=msgList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sessionManager=new SessionManager(context);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return msgList.size();
    }
    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    public class Holder
    {
        TextView tv;
        TextView txtRoomName,tvActivityTime;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.item_activity, null);
        holder.tv = (TextView) rowView.findViewById(R.id.txtItemBoard);
        holder.txtRoomName = (TextView) rowView.findViewById(R.id.txtRoomName);
        holder.img= (ImageView) rowView.findViewById(R.id.imgItemBoard);
        holder.tvActivityTime= (TextView) rowView.findViewById(R.id.tvActivityTime);

        holder.txtRoomName.setText(msgList.get(position).get(ROOM_NAME));
        holder.tv.setText(msgList.get(position).get(NOTIFICATION_MSG));

        holder.tvActivityTime.setText(msgList.get(position).get(ACTIVITY_TIME));

        if(!sessionManager.getDemoUser().equals("DemoUser")) {

            if (!msgList.get(position).get(USER_IMAGE).equals("")) {

                Picasso.with(context).invalidate("https://s3-us-west-2.amazonaws.com/" + msgList.get(position).get(USER_IMAGE));

                Picasso.with(context).load("https://s3-us-west-2.amazonaws.com/" + msgList.get(position).get(USER_IMAGE))
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .fetch();

                Picasso.with(context).load("https://s3-us-west-2.amazonaws.com/" + msgList.get(position).get(USER_IMAGE)).centerCrop()
                        .placeholder(R.drawable.profile).resize(100, 100)
                        .into(holder.img);
//            Picasso.with(context).load("https://s3-us-west-2.amazonaws.com/"+msgList.get(position).get("image"))
//                    //.placeholder(R.drawable.profile)
//                    .networkPolicy(NetworkPolicy.NO_CACHE).resize(80,80)
//                    .into(holder.img);
            } else {
                if (msgList.get(position).get(NOTIFICATION_MSG).contains("Physically switch")) {
                    holder.img.setImageResource(imageId[6]);
                } else {
                    holder.img.setImageResource(imageId[0]);
                }
            }
        }else{
            holder.img.setImageResource(imageId[position]);
        }

        return rowView;
    }

}
