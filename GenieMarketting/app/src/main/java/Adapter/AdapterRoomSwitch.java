package Adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.LivingRoom;
import com.genieiot.marketing.R;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Database.DatabaseHandler.TIME;;
import static Session.Constants.URL_GENIE;
import static Session.Constants.isInternetAvailable;
import static com.genieiot.marketing.LivingRoom.onSwitchCount;
import static com.genieiot.marketing.R.id.seekDimmer;
import static com.genieiot.marketing.R.id.switchSwitch;

//import static Database.DatabaseHandler.LOCK;

/**
 * Created by root on 9/20/16.
 */

public class AdapterRoomSwitch extends BaseAdapter {

    private ArrayList<HashMap<String, String>> switchDatas;
    Context context;
    DatabaseHandler db;
    private static LayoutInflater inflater = null;
    private boolean flag = false;
    String roomid = null;
    ArrayList<String> cnt = new ArrayList<String>();
    private boolean[] mCheckedState;
    SessionManager sp;
    private SessionManager sessionManager;
    LinearLayout llLinear;
    int step = 1;
    int max = 100;
    int min = 0;

    public AdapterRoomSwitch(Context context, ArrayList<HashMap<String, String>> switchDatas)
    {
        sp = new SessionManager(context);
        flag = false;
        this.switchDatas = switchDatas;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        db = new DatabaseHandler(context);
        mCheckedState = new boolean[switchDatas.size()];
        onSwitchCount = 0;
        switchDatas=db.getSwitches(ROOM_ID);
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return switchDatas.size();
    }
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return switchDatas.get(position);
    }
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    public class Holder {
        TextView tv;
        ImageView img, lock;
        SwitchCompat switchSwitch;
        LinearLayout layoutDimmer, llLinear;
        DiscreteSeekBar seekDimmer;
        TextView txtRangeValue;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.switch_item_second, null);
            holder = new Holder();
            holder.tv = (TextView) rowView.findViewById(R.id.txtItemSwitch);
            holder.llLinear= (LinearLayout) rowView.findViewById(R.id.llLinear);
            holder.txtRangeValue = (TextView) rowView.findViewById(R.id.txtRange);
            holder.img = (ImageView) rowView.findViewById(R.id.imgItemSwitch);
            holder.switchSwitch = (SwitchCompat) rowView.findViewById(switchSwitch);
            holder.tv.setText(switchDatas.get(position).get(SWITCH_NAME));
            holder.seekDimmer = (DiscreteSeekBar) rowView.findViewById(seekDimmer);
            holder.lock = (ImageView) rowView.findViewById(R.id.imglock);
            holder.layoutDimmer = (LinearLayout) rowView.findViewById(R.id.layoutDimmer);
            holder.llLinear = (LinearLayout) rowView.findViewById(R.id.llLinear);
//            holder.seekDimmer.setProgress(10);
//            holder.seekDimmer.setMax((max - min) / step);

            holder.switchSwitch.setOnCheckedChangeListener(null);
            //holder.switchSwitch.setCheckedImmediately(mSbStates.get(position));
            holder.seekDimmer.setTag(position);
            rowView.setTag(holder);

        } else {
            holder = (Holder) rowView.getTag();
        }

        holder.tv.setText(switchDatas.get(position).get(SWITCH_NAME));

        holder.switchSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(Constants.isInternetAvailable(context))
                {
                    String mDimmerValue=switchDatas.get(position).get(DIMMER_VALUE);
                    View view=holder.switchSwitch;
                   new SwitchOnOffAsyncTask(view).execute(position+"",mDimmerValue);
                }
                else
                {
                    Toast.makeText(context,"Please check net connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.switchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                try
                {
                    mCheckedState[position] = isChecked;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d("OnCheckedListner ",position+" "+isChecked);

                if (isInternetAvailable(context))
                {
                    switchButtonAction(position, isChecked, holder);
                }
            }
        });

        try
        {
            holder.switchSwitch.setChecked(mCheckedState[position]);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        holder.seekDimmer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//
//            int progress = 0;
//            int finalDimmerValue;
//
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//
//                this.progress = progress;
//                this.progress = min + (progress * step);
//                finalDimmerValue=69-((progress*66)/100);
//                holder.txtRangeValue.setText(this.progress+"");
//                Log.i("Dimmer Test ","Showing value :"+this.progress+"| Actual Value: "+finalDimmerValue);
//
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar)
//            {
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//
//                String switchIds = switchDatas.get(position).get(SWITCH_ID);
//                db.updateDimmerValue(String.valueOf(finalDimmerValue), switchIds);
//                View view=holder.switchSwitch;
//                if(isInternetAvailable(context)){
//                    HashMap<String,String> mMap=switchDatas.get(position);
//                    if(progress>0) {
//                        holder.switchSwitch.setChecked(true);
//                        mMap.put(SWITCH_STATUS,"1");
//                        Log.d("Switch Info",""+mMap);
//                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"1",finalDimmerValue+"");
//                        switchDatas.set(position,mMap);
//                        new SwitchOnOffAsyncTask(view).execute(position + "", finalDimmerValue + "");
//                    }else{
//                        mMap.put(SWITCH_STATUS,"0");
//                        Log.d("Switch Info",""+mMap);
//                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"0",finalDimmerValue+"");
//                        switchDatas.set(position,mMap);
//                        holder.switchSwitch.setChecked(false);
//                        new SwitchOnOffAsyncTask(view).execute(position+"",finalDimmerValue+"");
//                    }
//                }else{
//                    Toast.makeText(context,"Please check internet Connection.", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        holder.seekDimmer.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            int progress = 0;
            int finalDimmerValue;
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int progress, boolean fromUser) {
                 this.progress = progress;
                this.progress = min + (progress * step);
                finalDimmerValue=69-((progress*66)/100);
                //holder.txtRangeValue.setText(this.progress+"");
                Log.i("Dimmer Test ","Showing value :"+this.progress+"| Actual Value: "+finalDimmerValue);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

                String switchIds = switchDatas.get(position).get(SWITCH_ID);
                db.updateDimmerValue(String.valueOf(finalDimmerValue), switchIds);
                View view=holder.switchSwitch;
                if(isInternetAvailable(context)){
                    HashMap<String,String> mMap=switchDatas.get(position);
                    if(progress>0) {
                        holder.switchSwitch.setChecked(true);
                        mMap.put(SWITCH_STATUS,"1");
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"1",finalDimmerValue+"");
                        switchDatas.set(position,mMap);
                        new SwitchOnOffAsyncTask(view).execute(position + "", finalDimmerValue + "");
                    }else{
                        mMap.put(SWITCH_STATUS,"0");
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"0",finalDimmerValue+"");
                        switchDatas.set(position,mMap);
                        holder.switchSwitch.setChecked(false);
                        new SwitchOnOffAsyncTask(view).execute(position+"",finalDimmerValue+"");
                    }
                }else{
                    Toast.makeText(context,"Please check internet Connection.", Toast.LENGTH_SHORT).show();
                }



            }
        });

        if (switchDatas.get(position).get(DIMMER_STATUS).equals("1")) {

            holder.layoutDimmer.setVisibility(View.VISIBLE);
            int dimmervalueset=Integer.parseInt(switchDatas.get(position).get(DIMMER_VALUE).toString());
            int curval=101-(((dimmervalueset*100)/66)-3);
            holder.seekDimmer.setProgress(curval);
            holder.seekDimmer.setIndicatorPopupEnabled(true);
        } else {
            holder.seekDimmer.setIndicatorPopupEnabled(false);
            holder.layoutDimmer.setVisibility(View.GONE);

        }

        if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {

            //holder.img.setImageResource(R.drawable.table_lamp_off);
//                holder.switchSwitch.setChecked(false);
        } else {
            //holder.img.setImageResource(R.drawable.table_lamp);
//                holder.switchSwitch.setChecked(true);
        }

        setSwitchEvent(holder, position);
        if (getCount() - 1 == position)
        {
            flag = true;
        }

        if (switchDatas.get(position).get(LOCK).toString().equals("1"))
        {
            holder.lock.setVisibility(View.VISIBLE);
            //holder.switchSwitch.setEnabled(false);
        }
        else
        {
            //holder.switchSwitch.setEnabled(true);
            holder.lock.setVisibility(View.INVISIBLE);
        }

        if (sp.getUserType().equals("2"))
        { holder.lock.setEnabled(false); }
        else
        { holder.lock.setEnabled(true);  }

        return rowView;
    }

    private void setSwitchEventToggle(Holder holder, int position, boolean isChecked) {

        if (isInternetAvailable(context))
        {
            if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("1")) {

                if (!isChecked) {

                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.cfl_bulb_off);
                }
                else
                {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.cfl_bulb);
                }
            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("2")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.cfl_bulb_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.cfl_bulb);
                }

            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("3")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.cfl_bulb_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.cfl_bulb);
                }
            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("4")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.cfl_bulb_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.cfl_bulb);
                }

            }
            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("5")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.ceiling_fan_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.ceiling_fan);
                }

            }
            //new
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("6")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.ceiling_fan_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.ceiling_fan);
                }

            }
            //new
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("7")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.geyser_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.geyser);
                }

            }
            //new
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("8")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.microwave_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.microwave);
                }

            }
            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("9")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.mosquito_machine_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.mosquito_machine);
                }

            }
            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("10")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.wall_socket_off);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.wall_socket);
                }

            }

            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("11")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.curtainsoff);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.curtains);
                }

            }

            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("12")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.ironoff);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.iron);
                }

            }
            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("13")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.washingmachineoff);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.washing_machine);
                }

            }

            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("14")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.airconditioneroff);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.air_conditioner);
                }

            }

            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("15")) {
                if (!isChecked) {

//                holder.switchSwitch.setChecked(false);
                    db.setSwitchStatus("0", switchDatas.get(position).get(SWITCH_ID));
                    holder.img.setImageResource(R.drawable.plug);
                } else {
                    db.setSwitchStatus("1", switchDatas.get(position).get(SWITCH_ID));
                    holder.switchSwitch.setChecked(true);
                    holder.img.setImageResource(R.drawable.plug);

                }

            }
            //new data
        }
    }

    private void insertRecent(int position, String mStatus) {

        Calendar calander;
        String time;
        calander = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        time= simpleDateFormat.format(calander.getTime());

        HashMap<String, String> switcch = new HashMap<String, String>();
        switcch.put(SWITCH_ID, switchDatas.get(position).get(SWITCH_ID));
        switcch.put(SWITCH_NAME, switchDatas.get(position).get(SWITCH_NAME));
        switcch.put(SWITCH_TYPE_ID, switchDatas.get(position).get(SWITCH_TYPE_ID));
        switcch.put(SWITCH_STATUS,mStatus);
        switcch.put(ROOM_ID, switchDatas.get(position).get(ROOM_ID));
        switcch.put(DIMMER_STATUS, switchDatas.get(position).get(DIMMER_STATUS));
        switcch.put(DIMMER_VALUE, switchDatas.get(position).get(DIMMER_VALUE));
        switcch.put(ROOM_NAME,switchDatas.get(position).get(ROOM_NAME));
        switcch.put(TIME,time);
        db.insertRecentSwitch((switcch));

    }

    private void setSwitchEvent(Holder holder, int position) {

            if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("1")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {

                    holder.img.setImageResource(R.drawable.cfl_bulb_off);
                    holder.switchSwitch.setChecked(false);
                } else {

                    //r.play();
                    holder.img.setImageResource(R.drawable.cfl_bulb);
                    holder.switchSwitch.setChecked(true);
                }

            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("2")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.cfl_bulb_off);
                    //  holder.switchSwitch.setChecked(false);
                } else {
                    holder.img.setImageResource(R.drawable.cfl_bulb);
                    holder.switchSwitch.setChecked(true);
                }

            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("3")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.cfl_bulb_off);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.cfl_bulb);
                    holder.switchSwitch.setChecked(true);
                }


            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("4")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.cfl_bulb_off);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.cfl_bulb);
                    holder.switchSwitch.setChecked(true);
                }
            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("5")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.ceiling_fan_off);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.ceiling_fan);
                    holder.switchSwitch.setChecked(true);
                }
            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("6")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.ceiling_fan_off);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.ceiling_fan);
                    holder.switchSwitch.setChecked(true);
                }

            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("7")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.geyser_off);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.geyser);
                    holder.switchSwitch.setChecked(true);
                }
            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("8")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.microwave_off);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.microwave);
                    holder.switchSwitch.setChecked(true);
                }
            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("9")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.mosquito_machine_off);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.mosquito_machine);
                    holder.switchSwitch.setChecked(true);
                }
            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("10")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.wall_socket_off);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.wall_socket);
                    holder.switchSwitch.setChecked(true);
                }
            }
            //new
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("11")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.curtainsoff);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.curtains);
                    holder.switchSwitch.setChecked(true);
                }
            }
            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("12")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.ironoff);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.iron);
                    holder.switchSwitch.setChecked(true);
                }
            }

            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("13")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.washingmachineoff);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.washing_machine);
                    holder.switchSwitch.setChecked(true);
                }
            }

            //new data
            else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("14")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.airconditioneroff);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.air_conditioner);
                    holder.switchSwitch.setChecked(true);
                }
            } else if (switchDatas.get(position).get(SWITCH_TYPE_ID).equals("15")) {
                if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
                    holder.img.setImageResource(R.drawable.plug);
                    holder.switchSwitch.setChecked(false);

                } else {

                    holder.img.setImageResource(R.drawable.plug);
                    holder.switchSwitch.setChecked(true);
                }
            }

    }

    public void switchButtonAction(int position, boolean isChecked, Holder holder) {
        sessionManager = new SessionManager(context);

        if (flag) {

            setSwitchEventToggle(holder, position, isChecked);
        }

        if (isChecked) {

            String i = switchDatas.get(position).get(SWITCH_ID);
            if (switchDatas.get(position).get(SWITCH_ID).toString().equals("1")) {

                if (sessionManager.getUSERID() != null) {
                    ///new LongOperation().execute("!" + "" + i + "*" + "1" + "#");
                    onSwitchCount = onSwitchCount + 1;
                }
            } else if (switchDatas.get(position).get(SWITCH_ID).toString().equals("3")) {

                if (sessionManager.getUSERID() != null) {
                    //new LongOperation().execute("!" + "" + i + "*" + "1" + "#");
                    onSwitchCount = onSwitchCount + 1;
                }
            } else if (switchDatas.get(position).get(SWITCH_ID).toString().equals("2")) {

                if (sessionManager.getUSERID() != null) {
                   // new LongOperation().execute("!" + "" + i + "*" + "1" + "#");
                    onSwitchCount = onSwitchCount + 1;
                }
            }

        } else {
            //Toast.makeText(context, " Call NO/OFF Web service "+position, Toast.LENGTH_SHORT).show();
            String i = switchDatas.get(position).get(SWITCH_ID);
            if (switchDatas.get(position).get(SWITCH_ID).toString().equals("1")) {

                if (sessionManager.getUSERID() != null) {
                    //new LongOperation().execute("!" + "" + i + "*" + "0" + "#");
                    if (onSwitchCount != 0) {
                        onSwitchCount = onSwitchCount - 1;
                    }
                }

            } else if (switchDatas.get(position).get(SWITCH_ID).toString().equals("3")) {

                if (sessionManager.getUSERID() != null) {
                    //new LongOperation().execute("!" + "" + i + "*" + "0" + "#");
                    if (onSwitchCount != 0) {
                        onSwitchCount = onSwitchCount - 1;
                    }


                } else if (switchDatas.get(position).get(SWITCH_ID).toString().equals("2")) {

                    //new LongOperation().execute("!" + "" + i + "*" + "0" + "#");
                    // new LongOperation().execute("R2_0\r\n");
                    if (onSwitchCount != 0) {
                        onSwitchCount = onSwitchCount - 1;
                    }
                }
                if (onSwitchCount != 0) {
                    onSwitchCount = onSwitchCount - 1;
                }

            }
        }

            roomid = switchDatas.get(position).get(ROOM_ID).toString();
            if (roomid != null)
            {
                switchDatas.clear();
                switchDatas = db.getSwitches(roomid);
            }

            onSwitchCount = 0;
            for (int i = 0; i < mCheckedState.length; i++) {
                if (mCheckedState[i] == true)
                    onSwitchCount++;

                if (context instanceof LivingRoom)
                {
                    ((LivingRoom) context).settextCount(onSwitchCount);
                }
            }
        }

    class SwitchOnOffAsyncTask extends AsyncTask<String,Void,String> {
        int curPosition;
       // Holder holder = new Holder();
        View view;
        SwitchOnOffAsyncTask(View view){
            this.view=view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.setEnabled(false);
        }

        @Override
        protected String doInBackground(String... params) {
            Constants request=new Constants();
            String mResponse=null;
            try {
                curPosition = Integer.parseInt(params[0]);
                String dimmerValue=params[1];
                JSONObject jMain= new JSONObject();
                jMain.put("switchId",switchDatas.get(curPosition).get(SWITCH_ID));
                jMain.put("switchStatus",switchDatas.get(curPosition).get(SWITCH_STATUS));
                jMain.put("dimmerValue",dimmerValue);
                jMain.put("userid", sessionManager.getUSERID());

                Log.d("JSON BODY :",jMain+"");
                mResponse=request.doPostRequest(URL_GENIE+"/switch/changestatus",jMain.toString(),  sessionManager.getSecurityToken());

                return mResponse;

            }
            catch(Exception e)
            {}

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result!=null) {
                try {
                    JSONObject jResult=new JSONObject(result);
                    Log.d("Change Status Result : ",result);
                    if(jResult.getString("status").equals("SUCCESS")){
                       JSONObject jData=new JSONObject(jResult.getString("result"));
                       HashMap<String,String> mMap=switchDatas.get(curPosition);
                       mMap.put(SWITCH_STATUS,jData.getString("state"));
                       Log.d("Switch Info",""+mMap);
                       db.updateSwitchStatus(mMap.get(SWITCH_ID),jData.getString("state"),jData.getString("dimmerValue"));
                       insertRecent(curPosition,jData.getString("state"));
                       switchDatas.set(curPosition,mMap);
                    }
                    else
                    {
                        if(jResult.getString("status").equals("FAILURE")){
                            if(jResult.getString("msg").equals("Already Lock")){

                                db.UpdateLockFlag(switchDatas.get(curPosition).get(ROOM_ID),switchDatas.get(curPosition).get(SWITCH_ID),"1");
                                Toast.makeText(context, jResult.getString("msg"), Toast.LENGTH_SHORT).show();

                            } else if(jResult.getString("msg").equals("Already Hidden")){
                             //   db.UpdateHideStatus(switchDatas.get(curPosition).get(ROOM_ID),switchDatas.get(curPosition).get(SWITCH_ID),"1");
                                Toast.makeText(context, jResult.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                            notifyDataSetChanged();

                        }else {
                            Toast.makeText(context, "Fail to change status.", Toast.LENGTH_SHORT).show();
                        }
                      Log.d("Change Status Result","Fail");
                    }

                } catch (Exception e) {
                }
            }else{
                Log.d("ChangeStatus","Result null");
            }
            view.setEnabled(true);
        }
    }
}

