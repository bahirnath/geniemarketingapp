package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.genieiot.marketing.R;

import java.util.ArrayList;
import java.util.HashMap;

import Database.DatabaseHandler;

import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SELECTED_SWITCH;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;

/**
 * Created by root on 17/1/17.
 */

public class SelectSwitchesAdapter extends RecyclerView.Adapter<SelectSwitchesAdapter.MyViewHolder> {

    DatabaseHandler db;
    Context context;
    ArrayList<HashMap<String, String>> arrayList;
    ArrayList<Integer> mIconArrayList;


    public SelectSwitchesAdapter(Context context, ArrayList<HashMap<String, String>> arrayList, ArrayList<Integer> mIconArrayList) {
        this.context=context;
        this.arrayList=arrayList;
        db=new DatabaseHandler(context);
        this.mIconArrayList=mIconArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.selectswitches, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SelectSwitchesAdapter.MyViewHolder holder, final int position) {

        holder.cbSwitchSelect.setOnCheckedChangeListener(null);

        holder.txtItemSwitch.setText(arrayList.get(position).get(SWITCH_NAME));
        holder.txtItemRoom.setText(arrayList.get(position).get(ROOM_NAME));

        holder.imgItemSwitch.setImageResource(mIconArrayList.get(Integer.parseInt(arrayList.get(position).get(SWITCH_IMAGE_ID))));

        if(arrayList.get(position).get(SELECTED_SWITCH).equals("1")) {
            holder.cbSwitchSelect.setChecked(true);
        }
        else {
            holder.cbSwitchSelect.setChecked(false);
        }

        holder.cbSwitchSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                HashMap<String,String> mMap=arrayList.get(position);

                if(isChecked) {
                    mMap.put(SELECTED_SWITCH,"1");
                }
                else {
                    mMap.put(SELECTED_SWITCH,"0");
                }

                arrayList.set(position,mMap);
            }
        });
    }

    public Object getItem() {
        // TODO Auto-generated method stub
        return arrayList;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imgItemSwitch;
        TextView txtItemSwitch,txtItemRoom;
        CheckBox cbSwitchSelect;
        public MyViewHolder(View itemView) {
            super(itemView);
            imgItemSwitch= (ImageView) itemView.findViewById(R.id.imgItemSwitch);
            txtItemSwitch= (TextView) itemView.findViewById(R.id.txtItemSwitch);
            txtItemRoom= (TextView) itemView.findViewById(R.id.txtItemRoom);
            cbSwitchSelect= (CheckBox) itemView.findViewById(R.id.cbSwitchSelect);
        }
    }
}
