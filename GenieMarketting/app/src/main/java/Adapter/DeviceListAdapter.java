package Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import Session.SessionManager;

/**
 * Created by root on 29/11/16.
 */

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.MyViewHolder>
{
    Context context;
    ArrayList<HashMap<String, String>> arrayList=new ArrayList<>();
    SessionManager sessionManager;

    public DeviceListAdapter(Context context, ArrayList<HashMap<String, String>> arrayList) {
        this.context=context;
        this.arrayList=arrayList;
        sessionManager=new SessionManager(context);

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.device_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position){

        holder.tvIP.setText(arrayList.get(position).get("IP"));
        holder.tvMac.setText(arrayList.get(position).get("MAC"));

        holder.cvRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              new AsyncSocketWrite().execute(position+"");
                //arrayList.get(position).get("IP")
            }
        });

        holder.btnStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncSocketWrite().execute(position+"");
            }
        });

        if(arrayList.get(position).get("FLAG").equals("1")){
            holder.btnStatus.setVisibility(View.VISIBLE);
        }else{
            holder.btnStatus.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvIP,tvMac;
        CardView cvRow;
        Button btnStatus;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvIP= (TextView) itemView.findViewById(R.id.tvIP);
            tvMac= (TextView) itemView.findViewById(R.id.tvMac);
            cvRow= (CardView) itemView.findViewById(R.id.cvRow);
            btnStatus= (Button) itemView.findViewById(R.id.btnStatus);
        }
    }

    class AsyncSocketWrite extends AsyncTask<String,Void,String>{
        ProgressDialog progress;
        String mFinalResult="";
        String mIP="";
        int position;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress=new ProgressDialog(context);
            progress.setMessage("Please wait...");
            progress.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Socket pingSocket = null;
            PrintWriter out = null;
            BufferedReader in = null;
            BufferedReader inFromServer = null;

            try {
                position=Integer.parseInt(params[0]);
                mIP=arrayList.get(position).get("IP");
                pingSocket = new Socket(mIP,23);

                out = new PrintWriter(pingSocket.getOutputStream(), true);

                out.println(":s"+sessionManager.getSSID()+","+sessionManager.getSPassword()+",");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {

                }

                out.println(":f");

               inFromServer = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
               String userInput;
                int pos=0;

                while ((userInput =inFromServer.readLine()) != null) {
                    if(pos==2 && userInput!=null){
                        System.out.println(userInput);
                        mFinalResult=userInput;
                        break;
                    }
                    pos=pos+1;
                }

                out.close();
                pingSocket.close();

            } catch (final IOException e) {
                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Check hotspot", Toast.LENGTH_SHORT).show();
                    }
                });
                return "";

            }

            return mFinalResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(progress!=null){
                progress.cancel();
            }
            if(!mFinalResult.equals("")){
                HashMap<String,String> mMap=arrayList.get(position);
                mMap.put("FLAG","0");
                arrayList.set(position,mMap);
            }
            notifyDataSetChanged();
            Toast.makeText(context,result,Toast.LENGTH_SHORT).show();
        }

    }

}
