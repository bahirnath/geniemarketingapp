package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.R;
import com.genieiot.marketing.ShareControlUserList;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import Session.IOnClickListner;
import Session.SessionManager;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by root on 14/11/16.
 */

public class ShareControlAdapter extends RecyclerView.Adapter<ShareControlAdapter.MyViewHolder> {

    Context context;
    ArrayList<HashMap<String, String>> arrayList;
    IOnClickListner listner;
    String userType;
    SessionManager session;

    public ShareControlAdapter(ShareControlUserList context, ArrayList<HashMap<String, String>> arrayList,String userType) {

        this.context=context;
        this.arrayList=arrayList;
        this.listner=context;
        this.userType =userType;
        session=new SessionManager(context);

    }



    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sharcontroladapter, parent, false);
        return new MyViewHolder(itemView);
    }


    public void onBindViewHolder(MyViewHolder holder, final int position)
    {

        holder.username.setText(arrayList.get(position).get("userName"));
        holder.usertype.setText(arrayList.get(position).get("userType"));

        if(arrayList.get(position).get("image").equals("")){

        }  else{

            Picasso.with(context).load("https://s3-us-west-2.amazonaws.com/"+arrayList.get(position).get("image"))
                    .placeholder(R.drawable.profile)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(holder.profile_image);
        }

        holder.imgEditSharedC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(session.getUSERID().equals(arrayList.get(position).get("id")))
                {
                    Toast.makeText(context, "You don't have a permission", Toast.LENGTH_SHORT).show();
                }
                else
                {
                  listner.onClickListner(arrayList.get(position));
                }
            }
        });

        if(userType.equals("0")){
            holder.imgEditSharedC.setVisibility(View.VISIBLE);
        }else{
            holder.imgEditSharedC.setVisibility(View.GONE);
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView username,usertype;
        ImageView imgEditSharedC;
        CircleImageView profile_image;
        public MyViewHolder(View itemView) {
            super(itemView);

            username= (TextView) itemView.findViewById(R.id.username);
            usertype= (TextView) itemView.findViewById(R.id.usertype);
            profile_image= (CircleImageView) itemView.findViewById(R.id.profile_image);
            imgEditSharedC= (ImageView) itemView.findViewById(R.id.imgEditSharedC);

        }
    }
}
