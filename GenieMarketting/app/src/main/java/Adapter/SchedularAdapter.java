package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.genieiot.marketing.AddNewTask;
import com.genieiot.marketing.R;

import java.util.ArrayList;
import java.util.HashMap;

import static Database.DatabaseHandler.SWITCH_NAME;

/**
 * Created by root on 29/11/16.
 */

public class SchedularAdapter extends RecyclerView.Adapter<SchedularAdapter.MyViewHolder>
{
    Context context;
    ArrayList<HashMap<String, String>> arrayList=new ArrayList<>();
    int mPosition;
    String mRoomName;
    AddNewTask thisInstance;

    public SchedularAdapter(Context context, ArrayList<HashMap<String, String>> arrayList) {
        this.context=context;
        this.thisInstance= (AddNewTask) context;
        this.arrayList=arrayList;

    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.schedularadapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SchedularAdapter.MyViewHolder holder, final int position){

        mRoomName=arrayList.get(position).get("RoomName");

        holder.txtSwitchNameStatus.setText(arrayList.get(position).get(SWITCH_NAME)+" (" +mRoomName+ ")");
        if(arrayList.get(position).get("SwitchStatus").equals("1"))
        {
            holder.txtSwitchStatus.setText("Turn ON");
        }
        else
        {
            holder.txtSwitchStatus.setText("Turn OFF");
        }
        holder.txtTime.setText(arrayList.get(position).get("Time"));
        holder.txtDate.setText(arrayList.get(position).get("Schedule_dates"));
        holder.mLinear.setOnClickListener(new View.OnClickListener() {
            @Override
          public void onClick(View v) {
                thisInstance.onClickOfScheduleList(arrayList.get(position));
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtSwitchNameStatus,txtSwitchStatus,txtTime,txtDate;
        LinearLayout mLinear;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtTime= (TextView) itemView.findViewById(R.id.txtTime);
            txtSwitchNameStatus= (TextView) itemView.findViewById(R.id.txtSwitchNameStatus);
            txtSwitchStatus= (TextView) itemView.findViewById(R.id.txtSwitchStatus);
            txtDate= (TextView) itemView.findViewById(R.id.txtDate);
            mLinear= (LinearLayout) itemView.findViewById(R.id.linear);

        }
    }

}
