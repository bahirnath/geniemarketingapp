package Adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.LivingRoom;
import com.genieiot.marketing.R;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.CREATED_BY;
import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Database.DatabaseHandler.TIME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static Session.Constants.isInternetAvailable;

/**
 * Created by root on 21/12/16.
 */

public class AdapterCustomSwitch extends RecyclerView.Adapter<AdapterCustomSwitch.MyViewHolder> {

    Context context;
    ArrayList<HashMap<String, String>> switchDatas;
    SessionManager session;
    private List<Boolean> mSbStates;
    int step = 1;
    int max = 100;
    int min = 0;
    DatabaseHandler db;
    LivingRoom thisLiving;
    private int mPosition;
    private ArrayList<HashMap<String,Integer>> switchONFF;

    public AdapterCustomSwitch(Context context, ArrayList<HashMap<String, String>> arrayList,ArrayList<HashMap<String,Integer>> map) {

        this.context=context;
        this.switchDatas=arrayList;
        session=new SessionManager(context);
        db=new DatabaseHandler(context);
        thisLiving= (LivingRoom)context;
        switchONFF=map;
        mSbStates = new ArrayList<>(getItemCount());
        for (int i = 0; i < getItemCount(); i++) {
            mSbStates.add(false);
        }
    }
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.switch_item_row, parent, false);
        return new MyViewHolder(itemView);
    }
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
        holder.btnSwitchButton.setOnCheckedChangeListener(null);
        holder.tvSwitchName.setText(switchDatas.get(position).get(SWITCH_NAME));

        if (switchDatas.get(position).get(DIMMER_STATUS).equals("1")) {
            holder.layoutDimmer.setVisibility(View.VISIBLE);
            try {
                int dimmervalueset = Integer.parseInt(switchDatas.get(position).get(DIMMER_VALUE).toString());
                int curval = Integer.parseInt(((dimmervalueset * 100) / 75) + "");
                holder.seekDimmer.setProgress(curval);
            }catch (Exception e){}
            holder.seekDimmer.setIndicatorPopupEnabled(true);
        } else {
            holder.seekDimmer.setIndicatorPopupEnabled(false);
            holder.layoutDimmer.setVisibility(View.GONE);
        }
        if (switchDatas.get(position).get(SWITCH_STATUS).equals("0")) {
            holder.btnSwitchButton.setChecked(false);
            holder.imgSwitchImage.setImageResource(switchONFF.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("OFF"));
        } else {
            holder.btnSwitchButton.setChecked(true);
            holder.imgSwitchImage.setImageResource(switchONFF.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("ON"));
        }

        if (switchDatas.get(position).get(LOCK).toString().equals("1"))
        {   holder.lock.setVisibility(View.VISIBLE);   }
        else
        {   holder.lock.setVisibility(View.INVISIBLE); }


        if (session.getUserType().equals("2"))
        { holder.lock.setEnabled(false); }
         else
        {
            holder.lock.setEnabled(true);
        }

        holder.btnSwitchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                mSbStates.set(position, isChecked);
                HashMap<String,String> mMap=switchDatas.get(position);
                if(isChecked){
                    mMap.put(SWITCH_STATUS,"1");
                }else{
                    mMap.put(SWITCH_STATUS,"0");
                }

                switchDatas.set(position,mMap);

                if(Constants.isInternetAvailable(context)) {
                    String mDimmerValue=switchDatas.get(position).get(DIMMER_VALUE);
                    View view=holder.btnSwitchButton;
                    new SwitchOnOffAsyncTask(view).execute(position+"",mDimmerValue);
                }
                else {
                    Toast.makeText(context,"Please check net connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.seekDimmer.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            int progress = 0;
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int progress, boolean fromUser) {
                this.progress = progress;
                this.progress = min + (progress * step);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {
                int finalDimmerValue=((seekBar.getProgress()*75)/100);
                String switchIds = switchDatas.get(position).get(SWITCH_ID);
                db.updateDimmerValue(finalDimmerValue+"",switchIds);
                View view=holder.btnSwitchButton;
                if(isInternetAvailable(context)){
                    HashMap<String,String> mMap=switchDatas.get(position);
                    if(progress>0) {
                        mMap.put(SWITCH_STATUS,"1");
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"1",finalDimmerValue+"");
                        switchDatas.set(position,mMap);
                        new SwitchOnOffAsyncTask(view).execute(position + "", finalDimmerValue + "");
                    }else{
                        mMap.put(SWITCH_STATUS,"0");
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"0",finalDimmerValue+"");
                        switchDatas.set(position,mMap);
                        new SwitchOnOffAsyncTask(view).execute(position+"",finalDimmerValue+"");
                    }
                }else{
                    Toast.makeText(context,"Please check internet Connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.llMainScreen.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setPosition(holder.getAdapterPosition());
                return false;
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return switchDatas.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return switchDatas.get(position);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        TextView tvSwitchName;
        ImageView imgSwitchImage, lock;
        SwitchCompat btnSwitchButton;
        LinearLayout layoutDimmer, llLinear;
        DiscreteSeekBar seekDimmer;
        TextView txtRangeValue;
        LinearLayout llMainScreen;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvSwitchName = (TextView) itemView.findViewById(R.id.tvSwitchName);
            llLinear= (LinearLayout) itemView.findViewById(R.id.llLinear);
            txtRangeValue = (TextView) itemView.findViewById(R.id.txtRange);
            imgSwitchImage = (ImageView) itemView.findViewById(R.id.imgSwitchImage);
            btnSwitchButton = (SwitchCompat) itemView.findViewById(R.id.btnSwitchButton);
            seekDimmer = (DiscreteSeekBar) itemView.findViewById(R.id.seekDimmer);
            lock = (ImageView) itemView.findViewById(R.id.imglock);
            layoutDimmer = (LinearLayout) itemView.findViewById(R.id.layoutDimmer);
            llLinear = (LinearLayout) itemView.findViewById(R.id.llLinear);
            llMainScreen = (LinearLayout) itemView.findViewById(R.id.llMainScreen);
            itemView.setOnCreateContextMenuListener(this);
        }
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0,R.id.edit, 0, "Edit");
            menu.add(0, R.id.hide, 0, "Hide");
            if(switchDatas.get(getAdapterPosition()).get(LOCK).equals("0")) {
                menu.add(0, R.id.lock, 0, "Lock");
            }else{
                menu.add(0,R.id.lock,0,"Unlock");
            }
            menu.add(0, R.id.schedule, 0, "Schedule");
        }
    }
    public int getPosition() {
        return mPosition;
    }
    public void setPosition(int position) {
        this.mPosition = position;
    }
    public void refreshSwitchView(ArrayList<HashMap<String,String>> switchDataInfo){
        switchDatas=switchDataInfo;
        notifyDataSetChanged();
    }

    class SwitchOnOffAsyncTask extends AsyncTask<String,Void,String> {
        int curPosition;
        View view;
        SwitchOnOffAsyncTask(View view){
            this.view=view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants request=new Constants();
            String mResponse=null;
            String messageType="";
            try {

                if(URL_GENIE.equals(URL_GENIE_AWS)){
                    messageType=INTERNET;
                }else{
                    messageType=LOCAL_HUB;
                }

                curPosition = Integer.parseInt(params[0]);
                String dimmerValue=params[1];
                JSONObject jMain= new JSONObject();
                jMain.put("switchId",switchDatas.get(curPosition).get(SWITCH_ID));
                jMain.put("switchStatus",switchDatas.get(curPosition).get(SWITCH_STATUS));
                jMain.put("dimmerValue",dimmerValue);
                jMain.put("userid", session.getUSERID());
                jMain.put("messageFrom",messageType);

                Log.d("JSON BODY :",jMain+"");
                mResponse=request.doPostRequest(URL_GENIE+"/switch/changestatus",jMain.toString(), session.getSecurityToken());

                return mResponse;

            }
            catch(Exception e)
            {}

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result!=null) {
                try {
                    JSONObject jResult=new JSONObject(result);
                    Log.d("Change Status Result : ",result);
                    if(jResult.getString("status").equals("SUCCESS")){
                        JSONObject jData=new JSONObject(jResult.getString("result"));
                        HashMap<String,String> mMap=switchDatas.get(curPosition);
                        mMap.put(SWITCH_STATUS,jData.getString("state"));
                        mMap.put(DIMMER_VALUE,jData.getString("dimmerValue"));
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),jData.getString("state"),jData.getString("dimmerValue"));
                        insertRecent(curPosition,jData.getString("state"));
                        switchDatas.set(curPosition,mMap);
                        notifyItemChanged(curPosition);
                    }
                    else
                    {
                        if(jResult.getString("status").equals("FAILURE")){
                            if(jResult.getString("msg").equals("Already Lock")){

                                db.UpdateLockFlag(switchDatas.get(curPosition).get(ROOM_ID),switchDatas.get(curPosition).get(SWITCH_ID),"1");
                                Toast.makeText(context, jResult.getString("msg"), Toast.LENGTH_SHORT).show();

                            } else if(jResult.getString("msg").equals("Already Hidden")){
                                db.UpdateHideStatus(switchDatas.get(curPosition).get(ROOM_ID),switchDatas.get(curPosition).get(SWITCH_ID),"1","0");
                                Toast.makeText(context, jResult.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                            notifyItemChanged(curPosition);

                        }else {
                            Toast.makeText(context, "Fail to change status.", Toast.LENGTH_SHORT).show();
                        }
                        Log.d("Change Status Result","Fail");
                    }

                } catch (Exception e) {
                }
            }else{
                Log.d("ChangeStatus","Result null");
            }
            //Switch Button Enable after getting a response
           // view.setEnabled(true);

            if (context instanceof LivingRoom)
            {
                ((LivingRoom) context).settextCount(0);
            }

           // notifyDataSetChanged();
          // notifyItemChanged(curPosition);
        }
    }

    private void insertRecent(int position, String mStatus) {

        Calendar calander;
        String time;
        calander = Calendar.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        time= simpleDateFormat.format(calander.getTime());

        HashMap<String, String> switcch = new HashMap<String, String>();
        switcch.put(SWITCH_ID, switchDatas.get(position).get(SWITCH_ID));
        switcch.put(SWITCH_NAME, switchDatas.get(position).get(SWITCH_NAME));
        switcch.put(SWITCH_TYPE_ID, switchDatas.get(position).get(SWITCH_TYPE_ID));
        switcch.put(SWITCH_STATUS,mStatus);
        switcch.put(ROOM_ID, switchDatas.get(position).get(ROOM_ID));
        switcch.put(DIMMER_STATUS, switchDatas.get(position).get(DIMMER_STATUS));
        switcch.put(DIMMER_VALUE, switchDatas.get(position).get(DIMMER_VALUE));
        switcch.put(ROOM_NAME,switchDatas.get(position).get(ROOM_NAME));
        switcch.put(SWITCH_IMAGE_ID,switchDatas.get(position).get(SWITCH_IMAGE_ID));
        switcch.put(TIME,time);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        String formattedDate=dateFormat.format(date);
        switcch.put(CREATED_BY,formattedDate);

        db.insertRecentSwitch((switcch));

    }
}