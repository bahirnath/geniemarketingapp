package Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Database.DatabaseHandler.TIME;
import static Session.Constants.URL_GENIE;
//import static com.genieiot.geniesmarthome.LivingRoom.onSwitchCount;
import static com.genieiot.marketing.R.id.switchSwitch;

/**
 * Created by root on 9/20/16.
 */

public class AdapterRecent extends BaseAdapter {

    private static final String TABLE_SWITCHES ="Switches" ;
    private ArrayList<HashMap<String, String>> switchDatas;
    String[] result;
    Context context;
    int[] imageId;
    DatabaseHandler db;
    private static LayoutInflater inflater = null;
    private boolean flag = false;
    String roomid = null;
    ArrayList<String> cnt = new ArrayList<String>();
    private boolean[] mCheckedState;
    SessionManager sp;
    LinearLayout llLinear;
    public static int onSwitchCount = 0;
    ArrayList<HashMap<String, Integer>> switchONOFFImage;


    public AdapterRecent(Context context, ArrayList<HashMap<String, String>> switchDatas,ArrayList<HashMap<String, Integer>> switchONOFFImage) {

        sp = new SessionManager(context);
        flag = false;
        this.switchDatas = switchDatas;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        db = new DatabaseHandler(context);
        mCheckedState = new boolean[switchDatas.size()];
        this.switchONOFFImage=switchONOFFImage;
        onSwitchCount = 0;

    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return switchDatas.size();
    }
    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }
    public class Holder {
        TextView tv,tv1,txtTime;
        ImageView img;
        SwitchCompat switchSwitch;
        LinearLayout  llLinear;
        TextView txtRangeValue;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder;
        View rowView = convertView;
        if (rowView == null)
        {
            rowView = inflater.inflate(R.layout.item_recent, null);
            holder = new Holder();
            holder.tv = (TextView) rowView.findViewById(R.id.txtItemSwitch);
            holder.llLinear= (LinearLayout) rowView.findViewById(R.id.llLinear);
            holder.txtRangeValue = (TextView) rowView.findViewById(R.id.txtRange);
            holder.img = (ImageView) rowView.findViewById(R.id.imgItemSwitch);
            holder.switchSwitch = (SwitchCompat) rowView.findViewById(switchSwitch);
            holder.tv.setText(switchDatas.get(position).get(SWITCH_NAME));
            holder.tv1= (TextView) rowView.findViewById(R.id.txtItemRoom);
            holder.txtTime= (TextView) rowView.findViewById(R.id.txtTime);
            rowView.setTag(holder);
        }
        else
        {
            holder = (Holder) rowView.getTag();
        }

        holder.tv1.setText(switchDatas.get(position).get(ROOM_NAME));
        String mTimeTag=calulateRecentTime(switchDatas.get(position).get(TIME));
        holder.txtTime.setText(mTimeTag);

            if (switchDatas.get(position).get(SWITCH_STATUS).equals("0"))
            {
                holder.switchSwitch.setChecked(false);
                //holder.img.setImageResource(R.drawable.table_lamp_off);
                holder.switchSwitch.setThumbResource(R.drawable.thumb_image1);
                holder.img.setImageResource(switchONOFFImage.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("OFF"));

            }
            else
            {
                holder.switchSwitch.setChecked(true);
                holder.switchSwitch.setThumbResource(R.drawable.thumb_image);
                holder.img.setImageResource(switchONOFFImage.get(Integer.parseInt(switchDatas.get(position).get(SWITCH_IMAGE_ID))).get("ON"));
            }

        if (sp.getUserType().equals("2"))
        {
            holder.switchSwitch.setEnabled(false);
        }
        else
        {
            holder.switchSwitch.setEnabled(true);

        }



        holder.switchSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int curPostion=position;

                if(holder.switchSwitch.isChecked()){
                    if(sp.getDemoUser().equals("DemoUser")) {
                        String strConnection = ":!200:!245:!" + switchDatas.get(position).get(SWITCH_TYPE_ID) + ":!1:!0:!168";
                        new LongOperation().execute(strConnection,switchDatas.get(position).get(IP));
                        HashMap<String,String> mMap=switchDatas.get(position);
                        mMap.put(SWITCH_STATUS,"1");
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"1",switchDatas.get(position).get(DIMMER_VALUE));
                        db.updateRecentSwitchStatus(mMap.get(SWITCH_ID),"1",switchDatas.get(position).get(DIMMER_VALUE));
                        switchDatas.set(position,mMap);
                        notifyDataSetChanged();
                    }else{
                        new SwitchOnOffAsyncTask().execute(curPostion + "", "0", "1");
                        onSwitchCount = onSwitchCount + 1;
                        holder.switchSwitch.setThumbResource(R.drawable.thumb_image);
                    }

                }else{
                    if(sp.getDemoUser().equals("DemoUser")){
                        String strConnection = ":!200:!245:!" + switchDatas.get(position).get(SWITCH_TYPE_ID) + ":!0:!0:!168";
                        new LongOperation().execute(strConnection,switchDatas.get(position).get(IP));

                        HashMap<String,String> mMap=switchDatas.get(position);
                        mMap.put(SWITCH_STATUS,"0");
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),"0",switchDatas.get(position).get(DIMMER_VALUE));
                        db.updateRecentSwitchStatus(mMap.get(SWITCH_ID),"0",switchDatas.get(position).get(DIMMER_VALUE));
                        switchDatas.set(position,mMap);
                        notifyDataSetChanged();
                    }else {
                        new SwitchOnOffAsyncTask().execute(curPostion + "", "0", "0");
                        onSwitchCount = onSwitchCount - 1;
                        holder.switchSwitch.setThumbResource(R.drawable.thumb_image1);
                    }

                }
            }
        });

        return rowView;
    }

    private String calulateRecentTime(String startDate) {
        String compareString="";
        Date d1 = null;
        Date d2 = null;
        Calendar calander = Calendar.getInstance();
        SimpleDateFormat formatS = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String stopDate= formatS.format(calander.getTime());
        try{

            d1 = formatS.parse(startDate);
            d2 = formatS.parse(stopDate);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            if(diffDays>1){
                return diffDays+"day ago";
            }else if(diffHours>1){
                return diffHours+" hours "+diffMinutes+" min ago";
            }else if(diffMinutes>1){
                return diffMinutes+" min ago";
            }else {
                return diffSeconds+" sec ago";
            }

        }catch(Exception e){}
        return compareString;
    }

    class SwitchOnOffAsyncTask extends AsyncTask<String,Void,String> {
        int curPosition;
        @Override
        protected String doInBackground(String... params) {
            Constants request=new Constants();
            String mResponse=null;
            try {
                curPosition = Integer.parseInt(params[0]);
                String dimmerStatus=params[1];
                JSONObject jMain= new JSONObject();
                jMain.put("switchId",switchDatas.get(curPosition).get(SWITCH_ID));
                jMain.put("switchStatus",params[2]);
                jMain.put("dimmerValue",dimmerStatus);
                jMain.put("userid", sp.getUSERID());

                Log.d("Recent JSON BODY :",jMain+"");
                mResponse=request.doPostRequest(URL_GENIE+"/switch/changestatus",jMain.toString(),sp.getSecurityToken());

                return mResponse;

            }catch(Exception e){}

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result!=null) {
                try {
                    JSONObject jResult=new JSONObject(result);
                    Log.d("Change Status Result : ",result);
                    if(jResult.getString("status").equals("SUCCESS")){
                        JSONObject jData=new JSONObject(jResult.getString("result"));
                        HashMap<String,String> mMap=switchDatas.get(curPosition);
                        mMap.put(SWITCH_STATUS,jData.getString("state"));
                        Log.d("Switch Info",""+mMap);
                        db.updateSwitchStatus(mMap.get(SWITCH_ID),jData.getString("state"),jData.getString("dimmerValue"));
                        db.updateRecentSwitchStatus(mMap.get(SWITCH_ID),jData.getString("state"),jData.getString("dimmerValue"));
                        switchDatas.set(curPosition,mMap);
                    }else{
                        Log.d("Change Status Result","Fail");
                    }

                } catch (Exception e) {
                }
            }else{
                Log.d("ChangeStatus","Result null");
            }
        }

    }


    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                Thread.sleep(150);
            }catch (Exception e){}
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("TryDemoSwitchAdapter ","onPostExecute()");
            notifyDataSetChanged();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String strConnection = params[0];
                String IP=params[1];
                Log.d("Recent ON/OFF",IP);
                setSwitchOn(strConnection,IP);

            } catch (final Exception e) {

                e.printStackTrace();

                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
            return null;
        }
    }
    private void setSwitchOn(final String connection,String IP) throws Exception {

        Socket pingSocket=null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            pingSocket = new Socket(IP, 23);
            out = new PrintWriter(pingSocket.getOutputStream(), true);

        } catch (final IOException e) {
            ((Activity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
            return;
        }

        out.println(connection);
        Log.d("Success ",connection);
        out.close();
        pingSocket.close();

    }

}
