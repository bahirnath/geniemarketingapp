package Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.genieiot.marketing.HideList;
import com.genieiot.marketing.R;

import java.util.ArrayList;
import java.util.HashMap;

import static Database.DatabaseHandler.ROOM_IMAGE_TYPE;
import static Database.DatabaseHandler.ROOM_NAME;

/**
 * Created by root on 3/1/17.
 */

public class RoomHideListAdapter extends RecyclerView.Adapter<RoomHideListAdapter.MyViewHolder> {


    private HideList context;
    private ArrayList<HashMap<String, String>> arrayList;
    int[] myImageList = new int[]{R.drawable.bedroom, R.drawable.livingroom,R.drawable.kitchen,R.drawable.bathroom,
            R.drawable.balcony,R.drawable.living_room ,R.drawable.terrace,R.drawable.stdyroom,R.drawable.kidsbedroom};

    public RoomHideListAdapter(HideList context, ArrayList<HashMap<String, String>> arrayList) {
        this.context=context;
        this.arrayList=arrayList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hide_room_item, parent, false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {
        holder.txtRoomName.setText(arrayList.get(position).get(ROOM_NAME));
        String mImage=arrayList.get(position).get(ROOM_IMAGE_TYPE);

        holder.mImgItemSwitch.setImageResource(myImageList[Integer.parseInt(mImage)]);

        holder.mTxtUnhide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.onClickListner(arrayList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mImgItemSwitch;
        TextView txtRoomName,mTxtUnhide;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            mImgItemSwitch= (ImageView) itemView.findViewById(R.id.imgItemSwitch);
            txtRoomName= (TextView) itemView.findViewById(R.id.txtRoomName);
            mTxtUnhide= (TextView) itemView.findViewById(R.id.mTxtUnhide);


        }
    }



}