package Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.genieiot.marketing.R;

import java.util.ArrayList;

import DataModel.SwitchData;

import static com.genieiot.marketing.R.id.editSwitch;

/**
 * Created by root on 9/20/16.
 */

public class AdapterManageSwitch extends BaseAdapter {
    private final ArrayList<SwitchData> switchDatas;
    String[] result;
    Context context;
    int[] imageId;
    private static LayoutInflater inflater = null;
    private ArrayList<String> categories;
    private String mSwitchId;
    private String mSwitchName;


    public AdapterManageSwitch(Context context, ArrayList<SwitchData> switchDatas) {
        this.switchDatas = switchDatas;
        this.context = context;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return switchDatas.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView tv;
        ImageView img;
        SwitchCompat switchSwitch;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.item_manage, null);
        holder.tv = (TextView) rowView.findViewById(R.id.txtItemSwitch);
        holder.img = (ImageView) rowView.findViewById(R.id.switchSwitch);
        holder.tv.setText(switchDatas.get(position).getSwitchName());

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSwitchId = switchDatas.get(position).getSwitchId();
                mSwitchName = switchDatas.get(position).getSwitchName();

                switchDialog();


            }
        });

        return rowView;
    }


    private void switchDialog() {
        // Create custom dialog object
        final Dialog dialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        // Include dialog.xml file
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_switch);


        dialog.setTitle("Edit Switch");


        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        final EditText edtSwitchName = (EditText) dialog.findViewById(editSwitch);

        Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
        edtSwitchName.setText(mSwitchName);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtSwitchName.getText().toString().length() < 1) {
                    Toast.makeText(context, "Enter Switch Name", Toast.LENGTH_SHORT).show();
                    return;
                }


                Toast.makeText(context, "Updated Successfully", Toast.LENGTH_SHORT).show();


                dialog.dismiss();

            }
        });

    }

    private void setSpinnerData() {
        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("Select");
        categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");
    }

}
